<?php

return [
    'adminEmail'                    => 'sup1@gootax.pro',
    'supportEmail'                  => 'sup1@gootax.pro',
    'user.passwordResetTokenExpire' => 3600,
];
