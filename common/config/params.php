<?php

return [
    'adminEmail'                    => 'api_phone_call@gootax.pro',
    'supportEmail'                  => 'api_phone_call@gootax.pro',
    'user.passwordResetTokenExpire' => 3600,
];
