<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class'     => 'yii\redis\Cache',
            'redis'     => [
                'hostname' => 'redis-cache.taxi.lcl',
                'port'     => 6379,
                'database' => 0,
            ],
            'keyPrefix' => 'apiphonecall',
        ],
        'db'    => [
            'class'               => 'yii\db\Connection',
            'charset'             => 'utf8',
            'tablePrefix'         => 'tbl_',
            'dsn'                 => 'mysql:host=mysql0.lcl;dbname=taxi',
            'username'            => 'taxi',
            'password'            => 'JndfkkeiUSh38Zksf',
            'enableSchemaCache'   => false,
            'schemaCacheDuration' => 3600,
        ],
    ],
];

