<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => '192.168.81.12',
                'port'     => 6379,
                'database' => 0,
            ],
        ],
        'db'    => [
            'class'             => 'yii\db\Connection',
            'charset'           => 'utf8',
            'tablePrefix'       => 'tbl_',
            'enableSchemaCache' => true,
            // configuration for the master
            'dsn'               => 'mysql:host=192.168.81.15;dbname=k_taxi',
            'username'          => 'daimond',
            'password'          => '123456',
            // common configuration for slaves
            'slaveConfig'       => [
                'username' => 'daimond',
                'password' => '123456',
                'charset'  => 'utf8',
            ],
            // list of slave configurations
            'slaves'            => [
                ['dsn' => 'mysql:host=192.168.81.14;dbname=k_taxi'],
            ],
        ],
    ],
];
