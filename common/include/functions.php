<?php

/**
 * functions.php file.
 * Global shorthand functions for commonly used Yii methods.
 */

/**
 * Returns the application instance.
 * @return \yii\web\Application
 */
function app()
{
    return Yii::$app;
}

/**
 * Dumps the given variable using CVarDumper::dumpAsString().
 *
 * @param mixed $var
 * @param int   $depth
 * @param bool  $highlight
 */
function dump($var, $depth = 10, $highlight = true)
{
    return \yii\helpers\VarDumper::dump($var, $depth, $highlight);
}

/**
 * Debug function with die() after
 * dd($var);
 */
function dd($var, $showDebugCallPath = false, $depth = 10, $highlight = true)
{
    if ($showDebugCallPath) {
        $db   = current(debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1));
        $dump = [
            'file' => $db['file'],
            'line' => $db['line'],
            'dump' => $var,
        ];
    } else {
        $dump = $var;
    }

    dump($dump, $depth, $highlight);
    die();
}

/**
 * Returns post data
 */
function post($name = null, $defaultValue = null)
{
    return app()->request->post($name, $defaultValue);
}

/**
 * Returns get data
 */
function get($name = null, $defaultValue = null)
{
    return app()->request->get($name, $defaultValue);
}

/**
 * Yii translate
 */
function t($category, $message, $params = [], $language = null)
{
    return Yii::t($category, $message, $params, $language);
}

/**
 * @return string Returns domain name
 */
function domainName()
{
    $host = $_SERVER['HTTP_HOST'];
    $host = explode('.', $host);
    $host = $host[0];

    return $host;
}


function session()
{
    return app()->getSession();
}


function getValue($var, $return = null)
{
    return isset($var) ? $var : $return;
}


// Generate a random character string
function rand_str($length = 25, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890')
{
    // Length of character list
    $chars_length = (strlen($chars) - 1);

    // Start our string
    $string = $chars{rand(0, $chars_length)};

    // Generate random string
    for ($i = 1; $i < $length; $i = strlen($string)) {
        // Grab a random character from our list
        $r = $chars{rand(0, $chars_length)};

        // Make sure the same two characters don't appear next to each other
        if ($r != $string{$i - 1}) {
            $string .= $r;
        }
    }

    return $string;
}

function phoneFilter($phone)
{
    return preg_replace("/[^0-9]/", '', $phone);
}


