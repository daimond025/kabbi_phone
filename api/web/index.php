<?php

//defined('YII_DEBUG') or define('YII_DEBUG', true);
require(__DIR__ . '/../../common/config/env.php');
require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');
require(__DIR__ . '/../../common/include/functions.php');


if (YII_ENV_PROD) {
    $config = yii\helpers\ArrayHelper::merge(
                    require(__DIR__ . '/../../common/config/main.php'), require(__DIR__ . '/../config/main.php')
    );
} elseif (YII_ENV_DEV) {
    $config = yii\helpers\ArrayHelper::merge(
                    require(__DIR__ . '/../../common/config/main-local.php'), require(__DIR__ . '/../config/main-local.php')
    );
}

$application = new yii\web\Application($config);
$application->run();
