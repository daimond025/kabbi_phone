<?php

namespace api\models\queue;

use api\models\call\ActiveCallRedis;
use api\models\redis\ActiveDispatcher;
use api\models\redis\ActiveQueueCall;
use api\models\redis\FreeDispatcherQueue;
use yii\db\Exception;

class QueueService
{

    /**
     * @param array           $scenario
     * @param ActiveCallRedis $call
     *
     * @return array
     * @throws Exception
     */
    public static function dispatcherAction($scenario, $call)
    {
        $tenantId = $call->tenantId;
        $cityId   = $call->cityId;

        $activeQueueCall = ActiveQueueCall::findOne($tenantId . '_' . $cityId);
        if (!$activeQueueCall) {
            $activeQueueCall           = new ActiveQueueCall();
            $activeQueueCall->cityId   = $cityId;
            $activeQueueCall->tenantId = $tenantId;
        }

        app()->get('apiLogger')->log('Ищу свободных операторов в филиале {cityId}', ['cityId' => $cityId]);

        $dispatchers = FreeDispatcherQueue::findOne($tenantId);
        if (!$dispatchers) {
            app()->get('apiLogger')->log('Нет свободных операторов');
            if ($activeQueueCall->rpush($call->id)) {

                app()->get('apiLogger')->log('Ставлю звонок {callId} в очередь', ['callId' => $call->id]);

                return [
                    'scenario' => $scenario,
                    'queueId'  => $activeQueueCall->queueCallId,
                ];
            }
            throw new Exception('error when saving the queue calls');
        }

        foreach ((array)$dispatchers->list as $dispatcherId) {
            $dispatcher = ActiveDispatcher::findOne($dispatcherId);
            if ($dispatcher) {
                app()->get('apiLogger')->log('Проверяю оператора {agent}', ['agent' => $dispatcher->sip_id]);
                if (in_array($cityId, $dispatcher->city_arr)) {
                    $dispatchers->lrem($dispatcherId);

                    app()->get('apiLogger')->log('Найден свободный оператор {agent}', ['agent' => $dispatcher->sip_id]);

                    return [
                        'scenario' => [
                            'action'  => 'connectAgent',
                            'agent'   => $dispatcher->sip_id,
                            'type'    => $scenario['type'] ?: 'r',
                            'timeout' => $scenario['timeout'] ?: '10',
                        ],
                        'queueId'  => null,
                    ];

                } else {
                    app()->get('apiLogger')->log('Оператор {agent} не подходит. Филиалы: {cityIds}',
                        ['agent' => $dispatcher->sip_id, 'cityIds' => implode(', ', $dispatcher->city_arr)]);
                }
            }

        }

        app()->get('apiLogger')->log('Нет свободных операторов в филиале {cityId}', ['cityId' => $cityId]);

        if ($activeQueueCall->rpush($call->id)) {

            app()->get('apiLogger')->log('Ставлю звонок {callId} в очередь', ['callId' => $call->id]);

            return [
                'scenario' => $scenario,
                'queueId'  => $activeQueueCall->queueCallId,
            ];
        } else {
            throw new Exception('error when saving the queue calls');
        }
    }
}