<?php

namespace app\models\order;

use app\models\tenant\Tenant;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_order_change_data".
 *
 * @property integer $change_id
 * @property integer $tenant_id
 * @property integer $order_id
 * @property string $change_field
 * @property string $change_object_id
 * @property string $change_object_type
 * @property string $change_subject
 * @property string $change_val
 * @property string $change_time
 *
 * @property Order $order
 * @property Tenant $tenant
 */
class OrderChangeData extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_order_change_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'order_id'], 'required'],
            [['tenant_id', 'order_id'], 'integer'],
            [['change_field', 'change_object_id', 'change_object_type'], 'string', 'max' => 45],
            [['change_subject'], 'string', 'max' => 100],
            [['change_val'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'change_id'          => 'Change ID',
            'tenant_id'          => 'Tenant ID',
            'order_id'           => 'Order ID',
            'change_field'       => 'Change Field',
            'change_object_id'   => 'Change Object ID',
            'change_object_type' => 'Change Object Type',
            'change_subject'     => 'Change Subject',
            'change_val'         => 'Change Val',
            'change_time'        => 'Change Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

}
