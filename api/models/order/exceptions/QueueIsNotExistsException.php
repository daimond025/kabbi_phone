<?php

namespace api\models\order\exceptions;

use yii\base\Exception;


/**
 * Class QueueIsNotExistsException
 * @package api\models\order\exceptions
 */
class QueueIsNotExistsException extends Exception
{

}
