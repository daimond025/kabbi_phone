<?php

namespace app\models\order;

use app\models\driver\Worker;
use app\models\tenant\TenantSetting;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_order".
 *
 * @property integer           $order_id
 * @property integer           $tenant_id
 * @property integer           $worker_id
 * @property integer           $car_id
 * @property integer           $city_id
 * @property integer           $tariff_id
 * @property integer           $user_id
 * @property integer           $user_create
 * @property integer           $status_id
 * @property integer           $user_modifed
 * @property integer           $company_id
 * @property integer           $parking_id
 * @property string            $address
 * @property string            $comment
 * @property string            $predv_price
 * @property string            $device
 * @property integer           $order_number
 * @property string            $payment
 * @property integer           $show_phone
 * @property integer           $create_time
 * @property integer           $status_time
 * @property integer           $time_to_client
 * @property string            $client_device_token
 * @property integer           $order_time
 * @property string            $predv_distance
 * @property integer           $predv_time
 * @property integer           call_warning_id
 * @property string            phone
 *
 * @property ClientReview[]    $clientReviews
 * @property Exchange[]        $exchanges
 * @property Car               $car
 * @property City              $city
 * @property ClientCompany     $company
 * @property Worker            $worker
 * @property OrderStatus       $status
 * @property Parking           $parking
 * @property TaxiTariff        $tariff
 * @property Tenant            $tenant
 * @property User              $userCreate
 * @property User              $userModifed
 * @property OrderChangeData[] $orderChangeDatas
 * @property OrderDetailCost[] $orderDetailCosts
 * @property OrderHasOption[]  $orderHasOptions
 * @property CarOption[]       $options
 * @property OrderTrack[]      $orderTracks
 * @property ParkingOrder[]    $parkingOrders
 */
class Order extends ActiveRecord
{

    const PICK_UP_TIME = 300;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['tenant_id', 'tariff_id', 'client_id', 'status_id', 'address', 'order_number', 'status_time'],
                'required',
            ],
            [
                [
                    'tenant_id',
                    'worker_id',
                    'car_id',
                    'city_id',
                    'tariff_id',
                    'client_id',
                    'user_create',
                    'status_id',
                    'user_modifed',
                    'company_id',
                    'parking_id',
                    'order_number',
                    'show_phone',
                    'create_time',
                    'status_time',
                    'time_to_client',
                    'order_time',
                    'predv_time',
                    'call_warning_id',
                ],
                'integer',
            ],
            [['address', 'comment', 'device', 'payment', 'client_device_token', 'phone'], 'string'],
            [['predv_price', 'predv_distance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'            => 'Order ID',
            'tenant_id'           => 'Tenant ID',
            'worker_id'           => 'Worker ID',
            'car_id'              => 'Car ID',
            'city_id'             => 'City ID',
            'tariff_id'           => 'Tariff ID',
            'client_id'           => 'Client ID',
            'user_create'         => 'User Create',
            'status_id'           => 'Status ID',
            'user_modifed'        => 'User Modifed',
            'company_id'          => 'Company ID',
            'parking_id'          => 'Parking ID',
            'address'             => 'Address',
            'comment'             => 'Comment',
            'predv_price'         => 'Predv Price',
            'device'              => 'Device',
            'order_number'        => 'Order Number',
            'payment'             => 'Payment',
            'show_phone'          => 'Show Phone',
            'create_time'         => 'Create Time',
            'status_time'         => 'Status Time',
            'time_to_client'      => 'Time To Client',
            'client_device_token' => 'Client Device Token',
            'order_time'          => 'Order Time',
            'predv_distance'      => 'Predv Distance',
            'predv_time'          => 'Predv Time',
            'call_warning_id'     => 'Call Warning Id',
            'phone'               => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(\app\models\worker\Worker::className(), ['worker_id' => 'worker_id']);
    }

    public static function getPickUp($tenantId, $cityId)
    {
        $pickUp = TenantSetting::getTenantSettingValueByName($tenantId, TenantSetting::SETTING_1, $cityId);
        if (empty($pickUp)) {
            $pickUp = self::PICK_UP_TIME;
        }

        return $pickUp;
    }

    public static function findOrderFromMysql($orderId)
    {
        $order = self::find()
            ->where([
                'order_id' => $orderId,
            ])
            ->with([
                'worker',
            ])
            ->asArray()
            ->one();

        return $order;
    }

}
