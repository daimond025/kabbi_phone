<?php

namespace api\models\phone;

use api\extensions\redis\string\ActiveRecord;


/**
 * Class PhoneScenarioRedis
 * @package app\models\phone
 *
 * @property string $line_id
 * @property string $tenant_id
 * @property string $tariff_id
 * @property string $city_id
 * @property string $phone
 * @property string $incoming_scenario
 * @property string $block
 */
class PhoneScenarioRedis extends ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->get('redis_phone_scenario');
    }

    public static function primaryKey()
    {
        return ['phone'];
    }

    public function attributes()
    {
        return ['line_id', 'tenant_id', 'tariff_id', 'city_id', 'phone', 'incoming_scenario', 'block'];
    }

    public function rules()
    {
        return [
            [['line_id', 'tenant_id', 'tariff_id', 'city_id', 'phone', 'incoming_scenario'], 'required'],
            [['block'], 'safe'],
        ];
    }

    public function afterFind()
    {
        $this->incoming_scenario = unserialize($this->incoming_scenario);
        parent::afterFind(); // TODO: Change the autogenerated stub
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->incoming_scenario = serialize($this->incoming_scenario);

            return true;
        }

        return false;
    }

    /**
     * Проверяет существование сценария $scenario
     *
     * @param string $scenario
     *
     * @return bool
     */
    public function checkScenario($scenario)
    {
        $incoming_scenario = (array)$this->incoming_scenario;
        foreach ($incoming_scenario as $item) {
            if ($item->action === $scenario) {
                return true;
            }
        }

        return false;
    }

    /**
     * Возвращает номер первого найденного сценария $scenario
     *
     * @param string $scenario
     *
     * @return int|null
     */
    public function getActNumByScenario($scenario)
    {
        $incoming_scenario = (array)$this->incoming_scenario;
        foreach ($incoming_scenario as $key => $item) {
            if ($item['action'] === $scenario) {
                return (int)($key + 1);
            }
        }

        return null;
    }

}