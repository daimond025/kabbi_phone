<?php

namespace app\models\phone;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_phone_line".
 *
 * @property integer $line_id
 * @property integer $tenant_id
 * @property string $phone
 * @property integer $tariff_id
 * @property integer $city_id
 * @property string $incoming_scenario
 *
 * @property City $city
 * @property TaxiTariff $tariff
 * @property Tenant $tenant
 */
class PhoneLine extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_phone_line';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'phone', 'tariff_id', 'city_id'], 'required'],
            [['tenant_id', 'tariff_id', 'city_id'], 'integer'],
            [['incoming_scenario'], 'string'],
            [['phone'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'line_id'           => 'Line ID',
            'tenant_id'         => 'Tenant ID',
            'phone'             => 'Phone',
            'tariff_id'         => 'Tariff ID',
            'city_id'           => 'City ID',
            'incoming_scenario' => 'Incoming Scenario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public static function getPhoneLine($tenantId, $cityId, $tariffId)
    {
        $phoneLineDataArr = self::find()
            ->where(["tenant_id" => $tenantId])
            ->andWhere(["city_id" => $cityId])
            ->andWhere(['block' => 0])
            ->asArray()
            ->all();

        var_dump($phoneLineDataArr);
        exit();
        if (count($phoneLineDataArr) == 0) {
            return null;
        }
        foreach ($phoneLineDataArr as $phoneLineData) {
            if ($phoneLineData["tariff_id"] == $tariffId) {
                return $phoneLineData["phone"];
            }
        }
        return null;
    }

    /**
     * Getting phone line by phone number
     * @param  $phone
     * @return PhoneLine
     */
    public static function getLineDataByPhone($phone)
    {
        $phoneLine = self::find()
            ->where(
                ['phone' => $phone]
            )
            ->one();
        return $phoneLine;
    }

    public function afterFind()
    {
        $this->incoming_scenario = unserialize($this->incoming_scenario);
        parent::afterFind(); // TODO: Change the autogenerated stub
    }
}