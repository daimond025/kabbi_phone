<?php

namespace api\models\phone;


use api\models\notify\NotifyService;
use app\models\client\Client;
use app\models\phone\PhoneLine;
use yii\db\Exception;

class PhoneService
{
    /**
     * @param $phone
     *
     * @return PhoneScenarioRedis|null
     * @throws \yii\db\Exception
     */
    public static function getPhoneScenario($phone)
    {
        $model = PhoneScenarioRedis::findOne($phone);

        if (!$model) {
            $phoneLine = PhoneLine::find()
                ->where(['phone' => $phone])
                ->one();

            if (!$phoneLine) {
                app()->get('apiLogger')->log('Телефонной линии с номером {phone} не существует в MySQL', ['phone' => $phone]);
                throw new Exception('phone lines doesn\'t exists in mysql');
            }

            $model             = new PhoneScenarioRedis();
            $model->attributes = $phoneLine->attributes;

            if (!$model->save()) {
                throw new Exception('error when saving the phone line');
            }
            app()->get('apiLogger')->log('Копирую телефонную линию с номером {phone} из MySQL в Redis', ['phone' => $phone]);
        }
        return $model;
    }

    //Получение сценария вызова для автотвечика
    public static function getInfoScenario($tenantId, $phone)
    {
        $clientOrder = Client::getClientOrders($tenantId, $phone);

        if (count($clientOrder) === 1) {
            $order    = $clientOrder[0] ?: null;
            $orderId  = $order['order_id'] ?: null;
            $statusId = $order['status']['status_id'] ?: null;

            if (empty($orderId) || empty($statusId)) {
                return [];
            }

            $notifyData = NotifyService::getData($tenantId, $orderId, $phone, $statusId);
            if (!empty($notifyData)) {
                $notifyData['action'] = 'doInfo';
                return $notifyData;
            }
        }

        return [];
    }
}