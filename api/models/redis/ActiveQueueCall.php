<?php

namespace api\models\redis;

use api\extensions\redis\lst\ActiveRecord;

/**
 * Class ActiveQueueCall
 *
 * @property string $cityId
 * @property  string $tenantId
 * @property  string $queueCallId
 *
 * @package api\models\redis
 */
class ActiveQueueCall extends ActiveRecord
{

    public static function getDb()
    {
        return \Yii::$app->get('redis_queue_call');
    }

    public static function primaryKey()
    {
        return ['queueCallId'];
    }


    public function rules()
    {
        return [
            ['queueCallId', 'required'],
            ['list', 'safe'],
        ];
    }

    public function getCityId()
    {
        $array = explode('_',$this->queueCallId,2);
        return end($array);
    }

    public function getTenantId()
    {
        $array = explode('_',$this->queueCallId,2);
        return reset($array);
    }

    public function setCityId($value)
    {
        $queueCallId = $this->tenantId . '_' . $value;
        $this->queueCallId = $queueCallId;
    }

    public function setTenantId($value)
    {
        $queueCallId =  $value. '_' . $this->cityId;
        $this->queueCallId = $queueCallId;
    }

}