<?php

namespace  api\models\redis;


use api\extensions\redis\hash\ActiveRecord;

class ClientActiveOrder extends ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->get('redis_client_active_orders');
    }

    public static function primaryKey()
    {
        return ['tenantId', 'phone'];
    }

    public function attributes()
    {
        return ['tenantId', 'phone', 'value'];
    }


    public function rules()
    {
        return [
            ['tenantId', 'required'],
            ['key', 'safe'],
        ];
    }
}