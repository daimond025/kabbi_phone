<?php

namespace api\models\redis;

use api\extensions\redis\string\ActiveRecord;

/**
 * Class ActiveDispatcher
 *
 * @property string $dispetcher_id
 * @property string $user_id
 * @property string $tenant_id
 * @property string $sip_id
 * @property array $city_arr
 * @property string $work_start
 * @property string $on_pause
 * @property array $pause_info
 * @property string $count_income_answered_calls
 * @property string $count_income_noanswered_calls
 * @property string $count_outcome_answered_calls
 * @property string $count_outcome_noanswered_calls
 * @property string $count_completed_orders
 * @property string $count_rejected_orders
 * @property string $count_income_calls
 * @property string $count_outcome_calls
 * @property string $summ_income_calls_time
 * @property string $summ_outcome_calls_time
 * @property array $summ_completed_arr
 * @property array $summ_rejected_arr
 *
 * @package api\models\redis
 */
class ActiveDispatcher extends ActiveRecord
{

    public static function getDb()
    {
        return \Yii::$app->get('redis_active_dispetcher');
    }

    public static function primaryKey()
    {
        return ['user_id'];
    }

    public function attributes()
    {
        return [
            'dispetcher_id',
            'user_id',
            'tenant_id',
            'sip_id',
            'city_arr',
            'work_start',
            'on_pause',
            'pause_info',
            'count_income_answered_calls',
            'count_income_noanswered_calls',
            'count_outcome_answered_calls',
            'count_outcome_noanswered_calls',
            'count_completed_orders',
            'count_rejected_orders',
            'count_income_calls',
            'count_outcome_calls',
            'summ_income_calls_time',
            'summ_outcome_calls_time',
            'summ_completed_arr',
            'summ_rejected_arr',
        ];
    }


    public function rules()
    {
        return [
            [
                [
                    'dispetcher_id',
                    'user_id',
                    'tenant_id',
                    'sip_id',
                    'city_arr',
                    'work_start',
                    'on_pause',
                ],
                'required',
            ],
            [
                [
                    'pause_info',
                    'count_income_answered_calls',
                    'count_income_noanswered_calls',
                    'count_outcome_answered_calls',
                    'count_outcome_noanswered_calls',
                    'count_completed_orders',
                    'count_rejected_orders',
                    'count_income_calls',
                    'count_outcome_calls',
                    'summ_income_calls_time',
                    'summ_outcome_calls_time',
                    'summ_completed_arr',
                    'summ_rejected_arr',
                ],
                'safe',
            ],
        ];
    }
}