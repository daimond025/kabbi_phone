<?php

namespace api\models\redis;

use api\extensions\redis\lst\ActiveRecord;

class FreeDispatcherQueue extends ActiveRecord
{

    public static function getDb()
    {
        return \Yii::$app->get('redis_free_dispetcher_queue');
    }

    public static function primaryKey()
    {
        return ['tenantId'];
    }


    public function rules()
    {
        return [
            ['tenantId', 'required'],
            ['list', 'safe'],
        ];
    }
}