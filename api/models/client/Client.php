<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client".
 *
 * @property integer $client_id
 * @property integer $tenant_id
 * @property integer $city_id
 * @property string $photo
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $email
 * @property integer $black_list
 * @property integer $priority
 * @property string $create_time
 * @property integer $active
 * @property integer $success_order
 * @property integer $fail_driver_order
 * @property integer $fail_client_order
 * @property string $birth
 * @property integer $password
 *
 * @property Tenant $tenant
 * @property ClientBalance[] $clientBalances
 * @property ClientHasBonus[] $clientHasBonuses
 * @property ClientBonus[] $bonuses
 * @property ClientHasCompany[] $clientHasCompanies
 * @property ClientCompany[] $companies
 * @property ClientPhone[] $clientPhones
 * @property ClientReview[] $clientReviews
 * @property ClientReviewRaiting[] $clientReviewRaitings
 * @property OrderChange[] $orderChanges
 * @property OrderHistory[] $orderHistories
 */
class Client extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id'], 'required'],
            [['tenant_id', 'city_id', 'black_list', 'priority', 'active', 'success_order', 'fail_driver_order', 'fail_client_order', 'password'], 'integer'],
            [['create_time', 'birth'], 'safe'],
            [['photo'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id'         => 'Client ID',
            'tenant_id'         => 'Tenant ID',
            'city_id'           => 'City ID',
            'photo'             => 'Photo',
            'last_name'         => 'Last Name',
            'name'              => 'Name',
            'second_name'       => 'Second Name',
            'email'             => 'Email',
            'black_list'        => 'Black List',
            'priority'          => 'Priority',
            'create_time'       => 'Create Time',
            'active'            => 'Active',
            'success_order'     => 'Success Order',
            'fail_driver_order' => 'Fail Driver Order',
            'fail_client_order' => 'Fail Client Order',
            'birth'             => 'Birth',
            'password'          => 'Password',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBalances()
    {
        return $this->hasMany(ClientBalance::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientHasBonuses()
    {
        return $this->hasMany(ClientHasBonus::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasMany(ClientBonus::className(), ['bonus_id' => 'bonus_id'])->viaTable('tbl_client_has_bonus', ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientHasCompanies()
    {
        return $this->hasMany(ClientHasCompany::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(ClientCompany::className(), ['company_id' => 'company_id'])->viaTable('tbl_client_has_company', ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientPhones()
    {
        return $this->hasMany(ClientPhone::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientReviews()
    {
        return $this->hasMany(ClientReview::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientReviewRaitings()
    {
        return $this->hasMany(ClientReviewRaiting::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderChanges()
    {
        return $this->hasMany(OrderChange::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories()
    {
        return $this->hasMany(OrderHistory::className(), ['client_id' => 'client_id']);
    }

    /**
     * Функиця возвращает массив текущих заказов клиента из редиса
     * @param string $tenantId
     * @param string $phone
     * @return array resultOrderArr
     */
    public static function getClientOrders($tenantId, $phone)
    {
        $clientOrders = \Yii::$app->redis_client_active_orders->executeCommand('hget', [$tenantId, $phone]);
        $resultOrderArr = array();
        if (!empty($clientOrders)) {
            $clientOrders = unserialize($clientOrders);
            if (!empty($clientOrders)) {
                foreach ($clientOrders as $key => $orderId) {
                    $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);
                    if (!empty($orderData)) {
                        $orderData = unserialize($orderData);
                        if (!empty($orderData)) {
                            $resultOrderArr[] = $orderData;
                        }
                    }
                }
            }
        }
        return $resultOrderArr;
    }

}
