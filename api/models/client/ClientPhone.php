<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client_phone".
 *
 * @property integer $phone_id
 * @property integer $client_id
 * @property string $value
 *
 * @property Client $client
 * @property Order[] $orders
 */
class ClientPhone extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client_phone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'value'], 'required'],
            [['client_id'], 'integer'],
            [['value'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone_id'  => 'Phone ID',
            'client_id' => 'Client ID',
            'value'     => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['phone_id' => 'phone_id']);
    }

    /**
     * Проверка клиента на черный список в рамках заказчика
     * @param string $tenantId
     * @param string $phone
     * @return boolean
     */
    public static function isBlackList($tenantId, $phone)
    {
        $clientData = ClientPhone::find()
                ->joinWith('client')
                ->where(["value" => $phone])
                ->andWhere(["tenant_id" => $tenantId])
                ->asArray()
                ->one();
        //Уже есть такой клиент
        if (!empty($clientData)) {
            if (!empty($clientData['client']['black_list'])) {
                return true;
            }
        }
        return false;
    }

}
