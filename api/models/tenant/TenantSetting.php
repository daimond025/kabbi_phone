<?php

namespace app\models\tenant;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_tenant_setting".
 *
 * @property integer $setting_id
 * @property integer $tenant_id
 * @property string $name
 * @property string $value
 *
 * @property Tenant $tenant
 */
class TenantSetting extends ActiveRecord
{

    const SETTING_1 = 'PICK_UP';
    const SETTING_2 = 'PRE_ORDER';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name', 'value'], 'required'],
            [['tenant_id'], 'integer'],
            [['name', 'value'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => 'Setting ID',
            'tenant_id'  => 'Tenant ID',
            'name'       => 'Name',
            'value'      => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * Получение настройки арендодатора по городу
     * @param  $tenantId
     * @param  $name
     * @param  $cityId
     * @return boolean
     */
    public static function getTenantSettingValueByName($tenantId, $name, $cityId)
    {
        $settingData = self::find()
                ->where(["tenant_id" => $tenantId])
                ->andWhere([
                    "name"    => $name,
                    "city_id" => $cityId
                ])
                ->asArray()
                ->one();
        if (!empty($settingData)) {
            return $settingData['value'];
        }
        return null;
    }

}
