<?php

namespace app\models\tenant;

use app\models\order\Order;
use app\models\order\OrderChangeData;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "{{%autocall_file}}".
 *
 * @property integer $file_id
 * @property integer $tenant_id
 * @property string  $key
 * @property string  $name
 * @property string  $group
 *
 * @property Tenant  $tenant
 */
class AutocallFile extends ActiveRecord
{

    const STATUS_NEW = 1;
    const STATUS_OFFER_ORDER = 2;
    const STATUS_DRIVER_REFUSED = 3;
    const STATUS_FREE = 4;
    const STATUS_NOPARKING = 5;
    const STATUS_NEW_PRE_ORDER = 6;
    const STATUS_GET_DRIVER = 17;
    const STATUS_CLIENT_NOTIFIED = 101;
    // Когда клиент не выходит разбить на 2, в orderfiles вместо вас ожидает говорим 103 - платное ожидание

    const STATUS_DRIVER_WAITING = 26;
    const STATUS_CLIENT_NOT_EXIT = 27;
    const STATUS_EXECUTING = 36;
    const STATUS_OVERDUE = 52;
    const STATUS_DRIVER_LATE = 54;
    const STATUS_EXECUTION_PRE = 55;
    const STATUS_MANUAL_MODE = 110;

    public $tenant_id;
    public $order_id;
    private $_order;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%autocall_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id'], 'integer'],
            [['key', 'name', 'group'], 'required'],
            [['group'], 'string'],
            [['key', 'name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file_id'   => 'File ID',
            'tenant_id' => 'Tenant ID',
            'key'       => 'Key',
            'name'      => 'Name',
            'group'     => 'Group',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * Возвращает список файлов в нужном порядке.
     * @return boolean|array
     * [
     *   0 => [
     *     'key' => 'taxi' Ключ соответствия с именем файла
     *     'name' => '76.wav' Имя файла
     *     'group' => 'phrases' Групповая принадлежность
     *   ]
     * ]
     */
    public function getFiles()
    {
        $order        = \Yii::$app->redis_orders_active->executeCommand('hget', [$this->tenant_id, $this->order_id]);
        $this->_order = unserialize($order);
        if (empty($order)) {
            $this->_order = Order::findOrderFromMysql($this->order_id);
        }

        if (empty($this->_order)) {
            return false;
        }


        $arFiles = [
            'welcome'     => [],
            'orderfiles'  => [],
            'repeatfiles' => [],
            'driverfile'  => [],
            'agentfile'   => [],
            'wrongkey'    => [],
            'successfile' => [],
            'cancel'      => [],
        ];

        //welcomeFiles
        $welcomeFiles         = self::getFromDb(['hello', 'taxi', 'cancel']);
        $arFiles['welcome'][] = is_array($welcomeFiles['hello']) ? $welcomeFiles['hello'] : null;
        $arFiles['welcome'][] = is_array($welcomeFiles['taxi']) ? $welcomeFiles['taxi'] : null;
        $arFiles['cancel'][]  = is_array($welcomeFiles['cancel']) ? $welcomeFiles['cancel'] : null;

        //Connect files
        $connectFiles          = self::getFromDb(['connect_driver']);
        $arFiles['driverfile'] = [
            $connectFiles['connect_driver'],
        ];
        $connectFiles          = self::getFromDb(['connect_dispetcher']);
        $arFiles['agentfile']  = [
            $connectFiles['connect_dispetcher'],
        ];
        //wrongKey files
        $connectFiles        = self::getFromDb(['worng_key']);
        $arFiles['wrongkey'] = [
            $connectFiles['worng_key'],
        ];

        //Массив статусов поиска авто
        $arLookingCarStatus = [
            self::STATUS_NEW,
            self::STATUS_OFFER_ORDER,
            self::STATUS_DRIVER_REFUSED,
            self::STATUS_FREE,
            self::STATUS_NOPARKING,
            self::STATUS_NEW_PRE_ORDER,
            self::STATUS_MANUAL_MODE,
        ];

        //Массив статусов принял заказ, приехал, либо опаздывает
        $arDriverStatus = [
            self::STATUS_GET_DRIVER,
            self::STATUS_DRIVER_WAITING,
            self::STATUS_EXECUTION_PRE,
            self::STATUS_CLIENT_NOT_EXIT,
        ];

        $arDriverStatusLate = [
            self::STATUS_DRIVER_LATE,
        ];

        $arOrderRejected = [39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51];


        //Повторный звонок клиента (когда ещё идет поиск авто)
        if (in_array($this->_order['status_id'], $arLookingCarStatus)) {
            $autoCallFiles            = self::getFromDb(['looking', 'service_reject']);
            $arFiles['orderfiles'][]  = is_array($autoCallFiles['looking']) ? $autoCallFiles['looking'] : null;
            $arFiles['repeatfiles'][] = is_array($autoCallFiles['service_reject']) ? $autoCallFiles['service_reject'] : null;
        } //Принял заказ, приехал, либо опаздывает
        elseif (in_array($this->_order['status_id'], $arDriverStatus)) {
            $car          = $this->_order['car'];
            $brand        = current(explode(' ', $car['name']));
            $car['color'] = strtolower($car['color']);
            //Разбираем гос. номер
            $arNumber = [];
            if (preg_match('/[0-9]{3,}/', $this->_order['car']['gos_number'], $matches)) {
                $length = strlen($matches[0]);
                for ($i = 0; $i < $length; $i++) {
                    $arNumber[] = $matches[0][$i];
                }
            }
            if ($this->_order['status_id'] == self::STATUS_CLIENT_NOT_EXIT) {
                $phrase = 'paid_expectation';
            } else {
                if ($this->_order['status_id'] != self::STATUS_DRIVER_WAITING) {
                    $phrase = 'drives';
                } else {
                    $phrase = 'waiting';
                }
            }


            $autoCallFiles = self::getFromDb([$phrase, $brand, 'color', $car['color'], 'number']);
            $brandFile     = $autoCallFiles[$brand];
            if (empty($brandFile)) {
                $brand      = 'foreign_car';
                $brandFiles = self::getFromDb([$brand]);
                $brandFile  = $brandFiles[$brand];
            }
            $arFiles['orderfiles'] = [
                $autoCallFiles[$phrase],
                $brandFile,
            ];

            if ($autoCallFiles[$car['color']]) {
                $carColor = [
                    $autoCallFiles['color'],
                    $autoCallFiles[$car['color']],
                ];
                $arFiles['orderfiles'] = array_merge($arFiles['orderfiles'], $carColor);
            }

            $carNumber             = [];
            foreach ($arNumber as $key) {
                $tempNumber = self::getFromDb([$key]);
                if (is_array($tempNumber)) {
                    $carNumber[] = array_pop($tempNumber);
                }
            }
            $arFiles['orderfiles'] = array_merge($arFiles['orderfiles'], [$autoCallFiles['number']], $carNumber);

            //Добавление ключей в зависимости от статуса
            if (($this->_order['status_id'] == self::STATUS_GET_DRIVER ||
                    $this->_order['status_id'] == self::STATUS_EXECUTION_PRE) &&
                !$this->isNotified()
            ) {
                if ($this->_order['time_to_client'] == 1) {
                    $this->_order['time_to_client'] = "one";
                }

                if ($this->_order['time_to_client'] == 2) {
                    $this->_order['time_to_client'] = "two";
                }
                $declensionWords       = $this->declension_words($this->_order['time_to_client'],
                    ['minute', 'minuty', 'minutes']);
                $timeToClient          = $this->_order['time_to_client'];
                $autoCallFiles         = self::getFromDb(['in', $timeToClient, $declensionWords]);
                if (count($autoCallFiles) === 3) {
                    $timePhrase            = [
                        $autoCallFiles['in'],
                        $autoCallFiles[$timeToClient],
                        $autoCallFiles[$declensionWords],
                    ];
                    $arFiles['orderfiles'] = array_merge($arFiles['orderfiles'], $timePhrase);
                }
            }
            if ($this->_order['status_id'] == self::STATUS_DRIVER_WAITING || $this->_order['status_id'] == self::STATUS_CLIENT_NOT_EXIT) {
                $autoCallFiles            = self::getFromDb(['leave', 'driver_service_reject']);
                $arFiles['orderfiles']    = array_merge($arFiles['orderfiles'], [$autoCallFiles['leave']]);
                $arFiles['repeatfiles'][] = $autoCallFiles['driver_service_reject'];
            } else {
                $autoCallFiles            = self::getFromDb(['godspeed', 'service_driver_reject']);
                $arFiles['orderfiles']    = array_merge($arFiles['orderfiles'], [$autoCallFiles['godspeed']]);
                $arFiles['repeatfiles'][] = $autoCallFiles['service_driver_reject'];
            }
        } //Повторный звонок клиента во время выполнения заказа
        elseif ($this->_order['status_id'] == self::STATUS_EXECUTING) {
            $autoCallFilesMix         = self::getFromDb(['execution', 'service']);
            $arFiles['orderfiles'][]  = is_array($autoCallFilesMix['execution']) ? $autoCallFilesMix['execution'] : null;
            $arFiles['repeatfiles'][] = is_array($autoCallFilesMix['service']) ? $autoCallFilesMix['service'] : null;
        } //нет машин (для просроченного заказа) - 52
        elseif ($this->_order['status_id'] == self::STATUS_OVERDUE) {
            $autoCallFilesMix         = self::getFromDb([
                'sorry',
                'to_prolong_hold_on_line',
                'order_prolongated',
                'reject',
            ]);
            $arFiles['orderfiles'][]  = is_array($autoCallFilesMix['sorry']) ? $autoCallFilesMix['sorry'] : null;
            $arFiles['repeatfiles'][] = is_array($autoCallFilesMix['to_prolong_hold_on_line']) ? $autoCallFilesMix['to_prolong_hold_on_line'] : null;
            $arFiles['successfile'][] = is_array($autoCallFilesMix['order_prolongated']) ? $autoCallFilesMix['order_prolongated'] : null;
            //водила опаздывает - 54
        } elseif ($this->_order['status_id'] == self::STATUS_DRIVER_LATE) {
            $autoCallFilesMix         = self::getFromDb(['driver_late', 'service_driver_reject']);
            $arFiles['orderfiles'][]  = is_array($autoCallFilesMix['driver_late']) ? $autoCallFilesMix['driver_late'] : null;
            $arFiles['repeatfiles'][] = is_array($autoCallFilesMix['service_driver_reject']) ? $autoCallFilesMix['service_driver_reject'] : null;
        } elseif (in_array($this->_order['status_id'], $arOrderRejected)) {
            $arFiles['orderfiles'][] = is_array($welcomeFiles['cancel']) ? $welcomeFiles['cancel'] : null;
        } else {
            return null;
        }

        return $arFiles;
    }

    /**
     * Получаем значеия из БД по полю key.
     *
     * @param array|string $keys
     *
     * @return array
     */
    public static function getFromDb($keys)
    {
        return self::find()->
        where(['key' => $keys])->
        asArray()->
        indexBy('key')->
        select(['key', 'name', 'group'])->
        all();
    }

    /**
     * Уведомлен ли пользователь о подъзде авто.
     * @return bool
     */
    private function isNotified()
    {
        $notifiredStatusId = OrderChangeData::find()
            ->where(['order_id' => $this->order_id])
            ->andWhere(['change_field' => 'status_id'])
            ->andWhere(['change_object_type' => 'order'])
            ->andWhere(['change_val' => '17'])
            ->one();
        if (isset($notifiredStatusId->change_id)) {
            return (new Query())
                ->from('tbl_order_change_data')
                ->where([
                    'order_id'     => $this->order_id,
                    'change_field' => 'status_id',
                    'change_val'   => self::STATUS_CLIENT_NOTIFIED,
                ])
                ->andWhere('change_id > :assigned_change_id', [':assigned_change_id' => $notifiredStatusId->change_id])
                ->exists();
        } else {
            return false;
        }
    }

    /**
     * Сколнение слов
     *
     * @param integer $num Число
     * @param array   $arWords Склонения ['minute', 'minuty', 'minutes']  минуту,минуты,минут
     *
     * @return string Слово
     */
    private function declension_words($num, $arWords)
    {
        $number = $num < 21 ? $num : (int)substr($num, -1);

        if ($number == 1 || $number == "one") {
            $w = $arWords[0];
        } elseif ($number > 1 && $number < 5) {
            $w = $arWords[1];
        } else {
            $w = $arWords[2];
        }

        return $w;
    }

}
