<?php
namespace api\models\notify;

use app\models\order\Order;
use app\models\phone\PhoneLine;
use app\models\tenant\AutocallFile;
use app\models\tenant\Tenant;
use app\models\tenant\TenantSetting;

class NotifyService
{
    public static function getData($tenantId, $orderId, $phone, $statusId)
    {
        $domain = Tenant::getDomain($tenantId);
        $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);
        $orderData = unserialize($orderData);
        if (empty($orderData)) {
            $orderData = Order::findOrderFromMysql($orderId);
        }
        if (isset($tenantId, $orderId, $phone, $orderData, $domain)) {
            $phoneLine = PhoneLine::getPhoneLine($tenantId, $orderData['city_id'], $orderData['tariff_id']);
            if (!$phoneLine) {
                return null;
            }
            $callCountTry = TenantSetting::getTenantSettingValueByName($tenantId, 'CALL_COUNT_TRY', $orderData['city_id']);
            $callWaitTimeOut = TenantSetting::getTenantSettingValueByName($tenantId, 'CALL_WAIT_TIMEOUT', $orderData['city_id']);
            $autoCall = new AutocallFile(['tenant_id' => $tenantId, 'order_id' => $orderId]);

            $files = $autoCall->getFiles();
            $filterFiles = self::formatAudioFiles($files);
            $notifyData = [
                'order_id'          => (string)$orderId,
                'status_id'         => (string)$statusId,
                'domain'            => (string)$domain,
                'did'               => (string)$phone,
                'welcome'           => (string)$filterFiles['welcome'],
                'orderfiles'        => (string)$filterFiles['orderfiles'],
                'repeatfiles'       => (string)$filterFiles['repeatfiles'],
                'driverfile'        => (string)$filterFiles['driverfile'],
                'agentfile'         => (string)$filterFiles['agentfile'],
                'wrongkey'          => (string)$filterFiles['wrongkey'],
                'successfile'       => (string)$filterFiles['successfile'],
                'cancel'            => (string)$filterFiles['cancel'],
                'driver_phone'      => isset($orderData['worker']['phone']) ? (string)$orderData['worker']['phone'] : null,
                'phone_line'        => (string)$phoneLine,
                'call_count_try'    => isset($callCountTry) ? (string)$callCountTry : '1',
                'call_wait_timeout' => isset($callWaitTimeOut) ? (string)$callWaitTimeOut : '20',
            ];
            return $notifyData;
        }
        return null;
    }

    public static function formatAudioFiles($fileArray)
    {
        $filterFiles = [
            'welcome'     => [],
            'orderfiles'  => [],
            'repeatfiles' => [],
            'driverfile'  => [],
            'agentfile'   => [],
            'wrongkey'    => [],
        ];
        if (is_array($fileArray)) {
            foreach ($fileArray as $key => $value) {
                $fileString = '';
                if (is_array($value)) {
                    foreach ($value as $file) {
                        if (is_array($file)) {
                            $fileName = str_replace('.wav', '', $file['name']);
                            $group = $file['group'];
                            $fileString = $fileString . 'ivr/' . $group . '/' . $fileName . ',';
                        }
                    }
                }
                if (!empty($fileString)) {
                    $fileString = substr($fileString, 0, -1);
                }
                $filterFiles[$key] = $fileString;
            }
        }
        return $filterFiles;
    }
}