<?php

namespace app\models\call;

use app\models\worker\Worker;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_call".
 *
 * @property integer $id
 * @property string  $uniqueid
 * @property integer $tenant_id
 * @property integer $order_id
 * @property integer $worker_id
 * @property integer $user_opertor_id
 * @property string  $type
 * @property string  $source
 * @property string  $destination
 * @property string  $destinationcontext
 * @property string  $channel
 * @property string  $destinationchannel
 * @property string  $lastapplication
 * @property string  $starttime
 * @property string  $answertime
 * @property string  $endtime
 * @property integer $duration
 * @property integer $billableseconds
 * @property string  $disposition
 * @property int     $phone_line_id
 * @property string  $amaflags
 * @property string  $create_time
 * @propery string $userfield
 *
 * @property Tenant  $tenant
 */
class Call extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_call';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'uniqueid',
                    'source',
                    'destination',
                    'channel',
                    'duration',
                    'billableseconds',
                    'disposition',
                ],
                'required',
            ],
            [
                [
                    'tenant_id',
                    'order_id',
                    'duration',
                    'billableseconds',
                    'phone_line_id',
                    'worker_id',
                    'user_opertor_id',
                ],
                'integer',
            ],
            [['tenant_id', 'type', 'phone_line_id', 'destinationchannel', 'destinationcontext'], 'safe'],
            [['type', 'userfield'], 'string'],
            [['starttime', 'answertime', 'endtime', 'create_time'], 'safe'],
            [['uniqueid'], 'string', 'max' => 60],
            [['source', 'destination', 'lastapplication'], 'string', 'max' => 20],
            [['channel', 'destinationchannel', 'disposition', 'amaflags'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => 'ID',
            'uniqueid'           => 'Uniqueid',
            'tenant_id'          => 'Tenant ID',
            'order_id'           => 'Order ID',
            'worker_id'          => 'Worker ID',
            'user_opertor_id'    => 'User Opertor ID',
            'type'               => 'Type',
            'source'             => 'Source',
            'destination'        => 'Destination',
            'destinationcontext' => 'Destinationcontext',
            'channel'            => 'Channel',
            'destinationchannel' => 'Destinationchannel',
            'lastapplication'    => 'Lastapplication',
            'starttime'          => 'Starttime',
            'answertime'         => 'Answertime',
            'endtime'            => 'Endtime',
            'duration'           => 'Duration',
            'billableseconds'    => 'Billableseconds',
            'disposition'        => 'Disposition',
            'amaflags'           => 'Amaflags',
            'create_time'        => 'Create Time',
            'userfield'          => 'Userfield',
            'phone_line_id'      => 'Phone Line Id',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (!isset($this->user_opertor_id)) {
            return;
        }
        $dispetcherData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$this->user_opertor_id]);
        $dispetcherData = unserialize($dispetcherData);
        if (empty($dispetcherData)) {
            return;
        }

        //Это был входящий вызов на оператора
        if ($this->type == "income" && isset($this->user_opertor_id)) {
            //Звонок состоялся
            if ($this->disposition == "ANSWERED") {
                $talkingTime = $this->billableseconds;
                // Увелич. счетчик принятных вызовов
                $dispetcherData['count_income_answered_calls'] += 1;
                // Увелич. суммарное время входящих вызовов
                $dispetcherData['summ_income_calls_time'] += $talkingTime;
            } else {
                $dispetcherData['count_income_noanswered_calls'] += 1;
            }
            $dispetcherData['count_income_calls'] += 1;
        }
        //Это был исходящий вызов от оператора
        if ($this->type == "outcome" && isset($this->user_opertor_id)) {
            //Звонок состоялся
            if ($this->disposition == "ANSWERED") {
                $talkingTime = $this->billableseconds;
                $dispetcherData['count_outcome_answered_calls'] += 1;
                // Увелич. суммарное время исходящий вызовов
                $dispetcherData['summ_outcome_calls_time'] += $talkingTime;
            } else {
                $dispetcherData['count_outcome_noanswered_calls'] += 1;
            }
            $dispetcherData['count_outcome_calls'] += 1;
        }
        $dispetcherData = serialize($dispetcherData);
        \Yii::$app->redis_active_dispetcher->executeCommand('SET', [$this->user_opertor_id, $dispetcherData]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOpertor()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_opertor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLine()
    {
        return $this->hasOne(PhoneLine::className(), ['line_id' => 'phone_line_id']);
    }
}