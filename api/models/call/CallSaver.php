<?php

namespace app\models\call;

use app\models\user\User;
use app\models\worker\Worker;
use Yii;
use app\models\phone\PhoneLine;
use app\models\tenant\Tenant;
use app\models\client\Client;
use app\models\user\UserDispetcher;
use yii\base\Exception;
use yii\base\Model;


class CallSaver extends Model
{

    /**
     * Prapare raw callData  from CDR and save as Call model
     *
     * @param array $callData
     *
     * @return boolean
     */
    public function saveCallToBase($callData)
    {
        $call = new Call();
        if (!is_array($callData)) {
            return false;
        }
        foreach ($callData as $callRow => $callRowValue) {
            if ($call->hasAttribute($callRow)) {
                $call->$callRow = $callRowValue;
            }
        }
        $call = self::fixCall($call);
        try {
            if (!$call->save()) {
//                Yii::error($callData);
//                Yii::error($call->errors);

                return false;
            }

            return true;
        } catch (Exception $e) {
//            Yii::error($callData);
//            Yii::error($e);

            return false;
        }
    }

    /**
     * Подправляем сырые данные CDR, связываем их бизнес логикой
     *
     * @param \app\models\call\Call $call
     *
     * @return \app\models\call\Call
     */
    public static function fixCall(Call $call)
    {
        /* //Особенности типов вызовов
          // Исходящий оператора
          //  'source' => 'op_795',
          // 'destination' => '79127629913',
          // 'callerid' => 'op_795',
          // 'destinationchannel' => 'SIP/yakov-test.v2.uatgootax.ru-000000bc',
          //
          // Исходящий автоинформатора
          // 'source' => 'notify',
          // 'destination' => '79127629913',
          // 'destinationcontext' => 'ORIGINATOR',
          // 'callerid' => '"notify@yakov-test.v2.uatgootax.ru" <notify>',
          // 'channel' => 'Local/79127629913@ORIGINATOR-00000007;2',
          // 'destinationchannel' => 'SIP/yakov-test.v2.uatgootax.ru-000000b9',
          //
          // Входящий Без оператора
          // 'source' => '+79127629913',
          //'destination' => '74996772106',
          // 'destinationcontext' => 'RINGING',
          //'callerid' => '"+79127629913" <+79127629913>',
          // 'destinationchannel' => '',
          // 'userfield' => '',
          //
          // Входящий С оператором
          // 'source' => '+79127629913',
          //'destination' => '74996772106',
          // 'destinationcontext' => 'CONNECTAGENT',
          // 'callerid' => '"+79127629913" <+79127629913>',
          //'userfield' => 'op_795@yakov-test.v2.uatgootax.ru',
          // 'destinationchannel' => 'SIP/yakov-test.v2.uatgootax.ru-000000c0',
          //
          // Входящий На автоинформатор
          //  'source' => '+79127629913',
          //'destination' => '74996772106',
          //'destinationcontext' => 'test-in',
          //'callerid' => '"+79127629913" <+79127629913>',
          // 'lastapplication' => 'Read',
          // 'lastdata' => 'RESULT,ivr/phrases/96,1,,1,5',
          //   'userfield' => '',
          //
          //Входящий с соединением с водилой
          // 'source' => '+79127629913',
          // 'destination' => '79127550706',
          // 'destinationcontext' => 'CONNECTDST',
          //  'callerid' => '"notify@yakov-test.v2.uatgootax.ru"
          //<+79127629913>',
          // 'userfield' => '',
          // 'destinationchannel' => 'SIP/yakov-test.v2.uatgootax.ru-000000b6',
         * */

        if ($call->userfield) {
            $customData = json_decode($call->userfield, true);
            if (is_array($customData)) {
                if ($customData['domain']) {
                    $call->tenant_id = Tenant::getIdbyDomain($customData['domain']);
                }
                if ($customData['operator']) {
                    $operatorInfo = $customData['operator'];
                    if (strstr($operatorInfo, '@')) {
                        $dogPosition           = strpos($operatorInfo, "@");
                        $userOperatorData      = substr($operatorInfo, 0, $dogPosition);
                        $call->user_opertor_id = UserDispetcher::getUserIdBySipData($userOperatorData);
                        if (!$call->tenant_id && $call->user_opertor_id) {
                            $userData = User::find()
                                ->where(["user_id" => $call->user_opertor_id])
                                ->one();
                            if ($userData && $userData->tenant_id) {
                                $call->tenant_id = $userData->tenant_id;
                            }
                        }
                    }
                }

            }
        }

        //Исходящий вызов: если звонит автоинформатор
        if ($call->source == "notify") {
            $call->type = "notify";
            //Исходящий вызов: если звонит  диспетчер
        } else {
            if (strstr($call->source, 'op_')) {
                $call->user_opertor_id = UserDispetcher::getUserIdBySipData($call->source);
                $call->source          = str_replace('op_', '', $call->source);
                $call->type            = "outcome";
                $call->destination     = str_replace('+', '', $call->destination);
                if (substr($call->destination, 0, 1) == "8" && strlen($call->destination) == 11) {
                    $call->destination = substr_replace($call->destination, 7, 0, 1);
                }
                //Входящий вызов
            } else {
                $call->source = str_replace('+', '', $call->source);
                $call->type   = "income";
                //Если это не соединение с водителем, то в call->destination лежит тел линия
                if ($call->destinationcontext !== "CONNECTDST") {
                    $phoneLine = PhoneLine::getLineDataByPhone($call->destination);
                    if ($phoneLine) {
                        $call->phone_line_id = $phoneLine->line_id;
                    }
                } else {
                    //Звонок зашел на водителя
                    $workerData = Worker::find()
                        ->where([
                            'phone'     => $call->destination,
                            'tenant_id' => $call->tenant_id,
                        ])
                        ->one();
                    if ($workerData) {
                        $call->worker_id = $workerData->worker_id;
                    }
                }
            }
        }
        //Ищем заказ
        $clientPhone = null;
        if ($call->type == "outcome" || $call->type == "notify") {
            $clientPhone = $call->destination;
        } else {
            if ($call->type == "income") {
                $clientPhone = $call->source;
            }
        }
        $clientOrders = Client::getClientOrders($call->tenant_id, $clientPhone);
        if (is_array($clientOrders) && count($clientOrders) == 1) {
            $currOrder = $clientOrders[0];
            if (isset($currOrder['status']['status_group'])) {
                if ($currOrder['status']['status_group'] !== "rejected" && $currOrder['status']['status_group'] !== "completed") {
                    $call->order_id = $currOrder['order_id'];
                }
            }
        }
        $call->create_time = time();

        return $call;
    }
}