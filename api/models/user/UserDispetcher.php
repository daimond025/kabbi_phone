<?php

namespace app\models\user;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_user_dispetcher".
 *
 * @property integer $dispetcher_id
 * @property integer $user_id
 * @property string $sip_id
 *
 * @property User $user
 */
class UserDispetcher extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user_dispetcher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sip_id'], 'required'],
            [['user_id'], 'integer'],
            [['sip_id'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dispetcher_id' => 'Dispetcher ID',
            'user_id'       => 'User ID',
            'sip_id'        => 'Sip ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    public static function getUserIdBySipData($sipData)
    {
        $sipData = $sipData . '@';
        $userDispetcher = self::find()
            ->where(['like', 'sip_id', $sipData])
            ->one();
        if ($userDispetcher) {
            return $userDispetcher->user_id;
        }
        return null;
    }
}