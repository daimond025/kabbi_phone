<?php

namespace app\models\user;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_user".
 *
 * @property integer $user_id
 * @property integer $tenant_id
 * @property integer $position_id
 * @property string $email
 * @property string $email_confirm
 * @property string $password
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $photo
 * @property integer $active
 * @property string $birth
 * @property string $address
 * @property string $create_time
 * @property string $auth_key
 * @property string $password_reset_token
 * @property integer $active_time
 * @property integer $role
 *
 * @property Order[] $orders
 * @property OrderHistory[] $orderHistories
 * @property OrderViews[] $orderViews
 * @property PublicPlaceModeration[] $publicPlaceModerations
 * @property PublicPlaceModerationAdd[] $publicPlaceModerationAdds
 * @property StreetModeration[] $streetModerations
 * @property StreetModerationAdd[] $streetModerationAdds
 * @property Support[] $supports
 * @property Tenant $tenant
 * @property UserPosition $position
 * @property UserDispetcher[] $userDispetchers
 * @property UserHasCity[] $userHasCities
 * @property UserWorkingTime[] $userWorkingTimes
 */
class User extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'position_id', 'email', 'email_confirm', 'password', 'auth_key', 'password_reset_token', 'active_time'], 'required'],
            [['tenant_id', 'position_id', 'active', 'active_time', 'role'], 'integer'],
            [['birth', 'create_time'], 'safe'],
            [['email'], 'string', 'max' => 20],
            [['email_confirm', 'last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['password', 'photo', 'address', 'auth_key', 'password_reset_token'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 15],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'tenant_id' => 'Tenant ID',
            'position_id' => 'Position ID',
            'email' => 'Email',
            'email_confirm' => 'Email Confirm',
            'password' => 'Password',
            'last_name' => 'Last Name',
            'name' => 'Name',
            'second_name' => 'Second Name',
            'phone' => 'Phone',
            'photo' => 'Photo',
            'active' => 'Active',
            'birth' => 'Birth',
            'address' => 'Address',
            'create_time' => 'Create Time',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'active_time' => 'Active Time',
            'role' => 'Role',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_modifed' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories()
    {
        return $this->hasMany(OrderHistory::className(), ['user_modifed' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderViews()
    {
        return $this->hasMany(OrderViews::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicPlaceModerations()
    {
        return $this->hasMany(PublicPlaceModeration::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicPlaceModerationAdds()
    {
        return $this->hasMany(PublicPlaceModerationAdd::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreetModerations()
    {
        return $this->hasMany(StreetModeration::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreetModerationAdds()
    {
        return $this->hasMany(StreetModerationAdd::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(UserPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDispetchers()
    {
        return $this->hasMany(UserDispetcher::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHasCities()
    {
        return $this->hasMany(UserHasCity::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWorkingTimes()
    {
        return $this->hasMany(UserWorkingTime::className(), ['user_id' => 'user_id']);
    }
}
