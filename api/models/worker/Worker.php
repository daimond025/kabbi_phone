<?php

namespace app\models\worker;

use app\models\tenant\Tenant;
use app\models\worker\TenantCompany;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_worker".
 *
 * @property integer $worker_id
 * @property integer $tenant_id
 * @property integer $city_id
 * @property integer $callsign
 * @property string $password
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $photo
 * @property string $device
 * @property string $device_token
 * @property string $lang
 * @property string $description
 * @property string $device_info
 * @property string $email
 * @property string $partnership
 * @property string $birthday
 * @property integer $block
 * @property integer $create_time
 * @property integer $tenant_company_id
 *
 * @property City $city
 * @property Tenant $tenant
 * @property WorkerActiveAboniment[] $workerActiveAboniments
 * @property WorkerBlock[] $workerBlocks
 * @property WorkerHasDocument[] $workerHasDocuments
 * @property WorkerHasPosition[] $workerHasPositions
 * @property Position[] $positions
 * @property WorkerRefuseOrder[] $workerRefuseOrders
 * @property WorkerReviewRating[] $workerReviewRatings
 */
class Worker extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_worker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [ ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'worker_id' => 'Worker ID',
            'tenant_id' => 'Tenant ID',
            'city_id' => 'City ID',
            'callsign' => 'Callsign',
            'password' => 'Password',
            'last_name' => 'Last Name',
            'name' => 'Name',
            'second_name' => 'Second Name',
            'phone' => 'Phone',
            'photo' => 'Photo',
            'device' => 'Device',
            'device_token' => 'Device Token',
            'lang' => 'Lang',
            'description' => 'Description',
            'device_info' => 'Device Info',
            'email' => 'Email',
            'partnership' => 'Partnership',
            'birthday' => 'Birthday',
            'block' => 'Block',
            'create_time' => 'Create Time',
        ];
    }


    public function getTenantCompany()
    {
        return $this->hasOne(TenantCompany::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
}
