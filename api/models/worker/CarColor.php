<?php

namespace app\models\worker;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_car_color".
 *
 * @property integer $color_id
 * @property string $name
 *
 * @property Car[] $cars
 */
class CarColor extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_car_color';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'color_id' => 'Color ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['color' => 'color_id']);
    }
}
