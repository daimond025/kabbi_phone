<?php

namespace app\models\worker;

use app\models\tenant\Tenant;
use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%tenant_company}}".
 *
 * @property integer        $tenant_company_id
 * @property integer        $tenant_id
 * @property string         $name
 * @property string         $phone
 * @property integer        $sort
 * @property integer        $block
 * @property integer        $logo
 * @property integer        $use_logo_company
 * @property string         $stripe_account
 * @property string         $street
 * @property string         $city
 * @property string         $city_code
 * @property string         $tax_number
 * @property string         $fax
 * @property string         $site
 * @property integer        $user_contact
 * @property integer        $report_id
 *

 */
class TenantCompany extends \yii\db\ActiveRecord
{

    public $cropParams;

    public $num;

    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [];
    }
}
