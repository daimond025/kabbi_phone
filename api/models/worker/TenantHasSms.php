<?php

namespace app\models\worker;


use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tbl_tenant_has_sms}}".
 *
 * @property string $id
 * @property integer $server_id
 * @property string $tenant_id
 * @property string $login
 * @property string $password
 * @property integer $active
 * @property string $sign
 * @property string $city_id
 * @property integer $default

 */
class TenantHasSms extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_sms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'active'], 'required'],
            [['server_id', 'tenant_id', 'active', 'city_id'], 'integer'],
            [['login', 'password', 'sign'], 'string', 'max' => 100],
            [['default'], 'in', 'range' => ['0','1']],
        ];
    }

    public function subRules()
    {
        $rules = [
            [['login', 'password', 'sign'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
        return array_merge(parent::rules(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('app', 'ID'),
            'server_id' => Yii::t('setting', 'Server'),
            'tenant_id' => Yii::t('app', 'Tenant ID'),
            'login'     => Yii::t('setting', 'Login'),
            'password'  => Yii::t('app', 'Password'),
            'active'    => Yii::t('setting', 'Active'),
            'sign'      => Yii::t('setting', 'Sign'),
            'city_id'   => Yii::t('app', 'City ID'),
            'default'   => Yii::t('app', 'Use default'),
        ];
    }
}
