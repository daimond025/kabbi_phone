<?php

namespace api\extensions\redis\hash;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\BaseActiveRecord;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;


/**
 * ActiveRecord is the base class for classes representing relational data in terms of objects.
 *
 * The following is an example model called `Customer`:
 *
 * ```php
 * class Customer extends \yii\redis\ActiveRecord
 * {
 *     public function attributes()
 *     {
 *         return ['id', 'name', 'address', 'registration_date'];
 *     }
 *
 *     public function primaryKey()
 *     {
 *         return ['id'];
 *     }
 * }
 * ```
 *
 * @author Artem Khikmatov <artem.khikmatov@mail.ru>
 * @since 2.0
 */
class ActiveRecord extends BaseActiveRecord
{
    /**
     * Returns the database connection used by this AR class.
     * By default, the "redis" application component is used as the database connection.
     * You may override this method if you want to use a different database connection.
     * @return Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('redis');
    }

    /**
     * @inheritdoc
     * @return ActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function find()
    {
        return Yii::createObject(ActiveQuery::className(), [get_called_class()]);
    }

    /**
     * Returns the primary key name(s) for this AR class.
     * This method should be overridden by child classes to define the primary key.
     *
     * Note that an array should be returned even when it is a single primary key.
     *
     * @return string the primary keys of this record.
     */
    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * Returns the list of all attribute names of the model.
     * This method must be overridden by child classes to define available attributes.
     * @return array list of attribute names.
     */
    public function attributes()
    {
        throw new InvalidConfigException('The attributes() method of redis ActiveRecord has to be implemented by child classes.');
    }

    /**
     * Declares prefix of the key that represents the keys that store this records in redis.
     * By default this method returns the class name as the table name by calling [[Inflector::camel2id()]].
     * For example, 'Customer' becomes 'customer', and 'OrderItem' becomes
     * 'order_item'. You may override this method if you want different key naming.
     * @return string the prefix to apply to all AR keys
     */
    public static function keyPrefix()
    {
        return Inflector::camel2id(StringHelper::basename(get_called_class()), '_');
    }


    public static function encode($data)
    {
        return serialize($data);
    }

    public static function decode($string)
    {
        return unserialize($string);
    }

    /**
     * @inheritdoc
     */
    public function insert($runValidation = true, $attributes = null)
    {
        if ($runValidation && !$this->validate($attributes)) {
            return false;
        }
        if (!$this->beforeSave(true)) {
            return false;
        }
        $values = $this->getDirtyAttributes($attributes);
        $primaryKeys = static::primaryKey();
        $pk     = reset($primaryKeys);

        if (empty($values[$pk])) {
            return false;
        }

        $model = static::findOne($values[$pk]);

        if ($model) {
            return false;
        }

        foreach (static::attributes() as $attribute) {
            if (!isset($values[$attribute])) {
                $values[$attribute] = null;
            }
        }

        $data = static::encode($values);

        $db = static::getDb();
        $db->executeCommand('SET', [$values[$pk], $data]);

        $changedAttributes = array_fill_keys(array_keys($values), null);
        $this->setOldAttributes($values);
        $this->afterSave(true, $changedAttributes);

        return true;
    }

    public function update($runValidation = true, $attributes = null)
    {
        if ($runValidation && !$this->validate($attributes)) {
            return false;
        }
        if (!$this->beforeSave(false)) {
            return false;
        }

        $values = $this->getDirtyAttributes($attributes);
        $primaryKeys = static::primaryKey();
        $pk     = reset($primaryKeys);

        $model = static::findOne($this->$pk);

        if (!$model) {
            return false;
        }

        foreach (static::attributes() as $attribute) {
            if (!isset($values[$attribute])) {
                $values[$attribute] = $model->oldAttributes[$attribute];
            }
        }

        $data = static::encode($values);

        $db = static::getDb();
        $db->executeCommand('SET', [$values[$pk], $data]);

        $changedAttributes = array_fill_keys(array_keys($values), null);
        $this->setOldAttributes($values);
        $this->afterSave(true, $changedAttributes);

        return true;
    }
}
