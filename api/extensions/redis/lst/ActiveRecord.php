<?php

namespace api\extensions\redis\lst;

use Yii;
use yii\base\NotSupportedException;
use yii\db\BaseActiveRecord;
use yii\db\Exception;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;


/**
 * ActiveRecord is the base class for classes representing relational data in terms of objects.
 *
 * The following is an example model called `Customer`:
 *
 * ```php
 * class Customer extends \yii\redis\ActiveRecord
 * {
 *     public function attributes()
 *     {
 *         return ['id', 'name', 'address', 'registration_date'];
 *     }
 *
 *     public function primaryKey()
 *     {
 *         return ['id'];
 *     }
 * }
 * ```
 *
 * @author Artem Khikmatov <artem.khikmatov@mail.ru>
 * @since 2.0
 */
class ActiveRecord extends BaseActiveRecord
{
    protected $push = [];

    /**
     * Returns the database connection used by this AR class.
     * By default, the "redis" application component is used as the database connection.
     * You may override this method if you want to use a different database connection.
     * @return Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('redis');
    }

    /**
     * @inheritdoc
     * @return ActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function find()
    {
        return Yii::createObject(ActiveQuery::className(), [get_called_class()]);
    }

    /**
     * Returns the primary key name(s) for this AR class.
     * This method should be overridden by child classes to define the primary key.
     *
     * Note that an array should be returned even when it is a single primary key.
     *
     * @return string the primary keys of this record.
     */
    public static function primaryKey()
    {
        return ['id'];
    }

    public function attributes()
    {
        return array_merge(static::primaryKey(), ['list']);
    }


    /**
     * Declares prefix of the key that represents the keys that store this records in redis.
     * By default this method returns the class name as the table name by calling [[Inflector::camel2id()]].
     * For example, 'Customer' becomes 'customer', and 'OrderItem' becomes
     * 'order_item'. You may override this method if you want different key naming.
     * @return string the prefix to apply to all AR keys
     */
    public static function keyPrefix()
    {
        return Inflector::camel2id(StringHelper::basename(get_called_class()), '_');
    }

    public function getRange()
    {
        return count($this->list);
    }

    public function getItem($num)
    {
        return $this->list[$num];
    }

    public function getFirstItem()
    {
        return $this->getItem(0);
    }

    public function getLastItem()
    {
        return $this->getItem($this->getRange());
    }


    public function insert($runValidation = true, $attributes = null)
    {
        throw new NotSupportedException('insert is currently not supported by redis ActiveRecord.');
    }


    public function update($runValidation = true, $attributeNames = null)
    {
        throw new NotSupportedException('update is currently not supported by redis ActiveRecord.');
    }


    public function save($runValidation = true, $attributeNames = null)
    {
        throw new NotSupportedException('save is currently not supported by redis ActiveRecord.');
    }


    /**
     * Добавление элемента в конец списка
     *
     * @param mixed $value
     *
     * @return bool
     * @throws Exception
     */
    public function rpush($value)
    {
        $db    = static::getDb();
        $primaryKeys = static::primaryKey();
        $pkKey = reset($primaryKeys);
        $pk    = $this->attributes[$pkKey];
        if ($db->executeCommand('RPUSH', [$pk, $value])) {
            $list         = $this->list;
            $list[]       = $value;
            $this->list   = $list;
            $this->push[] = $value;

            return true;
        }

        throw new Exception('connection error to the redis');
    }

    /**
     * Удаляет все совпадения по значению
     * @param $value
     *
     * @return bool
     * @throws Exception
     */
    public function lrem($value)
    {
        $db    = static::getDb();
        $primaryKeys = static::primaryKey();
        $pkKey = reset($primaryKeys);
        $pk    = $this->attributes[$pkKey];
        if ($db->executeCommand('LREM', [$pk, 0, $value])) {
            $list = (array)$this->list;
            foreach ($list as $key => $item) {
                if ($item == $value) {
                    unset($list[$key]);
                }
            }
            $this->list   = $list;
            $this->push[] = $value;

            return true;
        }

        throw new Exception('connection error to the redis');
    }
}
