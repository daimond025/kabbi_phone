<?php

namespace api\actions;

use app\models\tenant\Tenant;
use yii\base\Action;

class BaseAction extends Action
{
    const REQUEST_GET = 'get';
    const REQUEST_POST = 'post';

    const DO_HANGUP = 'doHangup';
    const DO_ANSWER = 'doAnswer';
    const DO_BLACK_LIST = 'doBlackList';
    const DO_INFO = 'doInfo';
    const DO_QUEUE = 'doQueue';
    const CONNECT_AGENT = 'connectAgent';

    protected $params = [];

    /**
     * Получение типа запроса
     * @return string
     */
    public function getTypeRequest()
    {
        return self::REQUEST_GET;
    }

    /**
     * Фильтрация входящих параметров
     *
     * @param array $params
     *
     * @return array
     */
    protected function filterParams(array $params)
    {
        $paramsFiltered = [];
        foreach ($params as $paramKey => $paramVal) {
            if (is_string($paramVal)) {
                $paramVal = urldecode($paramVal);
                $paramVal = mb_strcut($paramVal, 0, 512);
                $paramVal = str_replace('\\', '/', $paramVal);
                $paramVal = trim(strip_tags(stripcslashes($paramVal)));
            }
            $paramsFiltered[$paramKey] = $paramVal;
        }

        return $paramsFiltered;
    }

    protected function setParams()
    {
        $typeRequest = $this->getTypeRequest();
        switch ($typeRequest) {
            case self::REQUEST_POST:
                $params = \Yii::$app->request->post();
                break;
            case self::REQUEST_GET:
            default:
                $params = \Yii::$app->request->get();
        }

        unset($params['q']);
        $this->params = $this->filterParams($params);
    }

    protected function getJsonAnswer($result)
    {
        return json_encode($result);
    }

    protected function beforeRun()
    {
        if (parent::beforeRun()) {
            $this->setParams();

            return true;
        }

        return false;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    protected function getDomain ($tenantId)
    {
        return (string) Tenant::getDomain($tenantId);
    }

    protected function getIp()
    {
        $headersObject = app()->request->getHeaders();
        $headers = $headersObject->toArray();
        return isset($headers['x-real-ip'][0]) ? $headers['x-real-ip'][0] : $_SERVER['REMOTE_ADDR'];
    }
}