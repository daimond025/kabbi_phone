<?php

namespace app\actions;

use api\actions\BaseAction;
use api\exceptions\EmptyParamsException;
use api\exceptions\InternalErrorException;
use api\exceptions\UnknownPbxHostException;
use yii\web\HttpException;

class ErrorAction extends BaseAction
{
    public function run()
    {
        $response = [
            'status' => 'error',
            'message' => 'Unknown error',
        ];

        $exception = \Yii::$app->getErrorHandler()->exception;


        if ($exception instanceof EmptyParamsException) {
            $response['message'] = $exception->getMessage();


        } elseif ($exception instanceof InternalErrorException) {
            $response['message'] = $exception->getMessage();


        } elseif ($exception instanceof UnknownPbxHostException) {
            $response['message'] = $exception->getMessage();


        } elseif ($exception instanceof HttpException) {
            $response['message'] = 'Unknown method';


        }

        app()->get('apiLogger')->log('Response: {data}', ['data' => json_encode($response, JSON_UNESCAPED_SLASHES)]);

        return $this->getJsonAnswer($response);
    }

}