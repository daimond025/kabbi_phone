<?php

namespace api\actions\call;

use api\actions\BaseAction;

class PingAction extends BaseAction
{

    public function run()
    {
        $response = [
            'status' => 'ok',
            'time'   => time(),
        ];

        return $this->getJsonAnswer($response);
    }
}