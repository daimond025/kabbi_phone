<?php

namespace api\actions\call;

use api\actions\BaseAction;
use api\exceptions\EmptyParamsException;
use api\exceptions\InternalErrorException;
use api\exceptions\UnknownPbxHostException;
use api\models\call\ActiveCallRedis;
use api\models\phone\PhoneService;
use api\models\queue\QueueService;
use app\models\client\ClientPhone;
use yii\base\Exception;
use yii\db\Exception as DbException;

class IncomingAction extends BaseAction
{

    public function run()
    {
        $callId   = $this->params['callid'] ?: null;
        $dId      = $this->params['did'] ?: null;
        $dId      = str_replace('+', '', $dId);
        $clId     = $this->params['clid'] ?: null;
        $clId     = str_replace('+', '', $clId);
        $fromInfo = $this->params['frominfo'] ?: null;

        app()->get('apiLogger')->setCallId($callId);
        app()->get('apiLogger')->log('Входящий вызов');

        if (empty($callId)) {

            app()->get('apiLogger')->error('Не указан идентификатор звонка');

            throw new EmptyParamsException();
        }

        if (empty($dId)) {
            $response = ['action' => self::DO_HANGUP];

            app()->get('apiLogger')->error('Не указан номер телефонной линии');
            app()->get('apiLogger')->log('Response: {data}',
                ['data' => json_encode($response, JSON_UNESCAPED_SLASHES)]);

            return $this->getJsonAnswer($response);
        }

        app()->get('apiLogger')->log('Ищу телефонную линию в БД');

        try {
            $phoneScenario = PhoneService::getPhoneScenario($dId);

            app()->get('apiLogger')->log('Телефонная линия найдена');

        } catch (Exception $e) {
            $response = ['action' => self::DO_HANGUP];

            app()->get('apiLogger')->log('Телефонная линия не найдена');
            app()->get('apiLogger')->log('Response: {data}',
                ['data' => json_encode($response, JSON_UNESCAPED_SLASHES)]);

            return $this->getJsonAnswer($response);
        }

        $tenantDomain = $this->getDomain($phoneScenario->tenant_id);

        app()->get('apiLogger')->log('Ищу звонок в Redis\'е');

        $call = ActiveCallRedis::findOne($callId);

        if (!$call) {
            $pbxId            = explode('_', $callId, 2);
            $call             = new ActiveCallRedis();
            $call->attributes = [
                'id'       => $callId,
                'did'      => $dId,
                'clid'     => $clId,
                'scenario' => (array)$phoneScenario->incoming_scenario,
                'queue'    => null,
                'actNum'   => 1,
                'pbxId'    => reset($pbxId),
                'tenantId' => $phoneScenario->tenant_id,
                'orderId'  => null,
                'cityId'   => $phoneScenario->city_id,
            ];


            if (array_key_exists($call->pbxId, \Yii::$app->params['pbxHost'])) {
                $call->pbxHost = \Yii::$app->params['pbxHost'][$call->pbxId];
            } else {
                throw new UnknownPbxHostException();
            }
            app()->get('apiLogger')->log('Звонок не найден. Создаю новый звонок в Redis\'e');
        } else {
            app()->get('apiLogger')->log('Звонок найден');
        }
        app()->get('apiLogger')->log('call = {call}',
            ['call' => json_encode($call->attributes, JSON_UNESCAPED_SLASHES)]);

        app()->get('apiLogger')->log('Сценарий {scenario}', ['scenario' => $call->firstScenario['action']]);
        // Если телефон скрыт и есть сценарий DO_ANSWER от отправляем его, иначе разрываем соединение
        if (empty($clId)) {
            $actNum = $phoneScenario->getActNumByScenario(self::DO_ANSWER);

            app()->apiLogger->log('Телефонный номер клиента скрыт');
            if ($actNum === null) {
                $response = ['action' => self::DO_HANGUP];

                app()->apiLogger->log('Нет сценария DO_ANSWER');
                app()->apiLogger->log('Разрываю соединение');

                app()->get('apiLogger')->log('Response: {data}',
                    ['data' => json_encode($response, JSON_UNESCAPED_SLASHES)]);

                return $this->getJsonAnswer($response);
            } else {
                $call->actNum = $actNum;
                $call->save();
                $response = [
                    'action' => self::DO_ANSWER,
                    'domain' => $tenantDomain,
                ];

                app()->apiLogger->log('Сценарий DO_ANSWER');
                app()->get('apiLogger')->log('Response: {data}',
                    ['data' => json_encode($response, JSON_UNESCAPED_SLASHES)]);

                return $this->getJsonAnswer($response);
            }
        }


        if ($fromInfo == 1) {

            foreach ($call->scenario as $scenario) {
                $call->actNum++;
                if ($scenario['action'] == self::DO_QUEUE) {
                    app()->apiLogger->log('Сценарий DO_QUEUE');
                    $resultDispatcherAction                       = QueueService::dispatcherAction($scenario, $call);
                    $resultDispatcherAction['scenario']['domain'] = $tenantDomain;

                    if (isset($resultDispatcherAction['queueId'])) {
                        $call->queue = $resultDispatcherAction['queueId'];
                    }

                    if (!$call->save()) {
                        throw new DbException('error when saving the queue calls');
                    }

                    app()->get('apiLogger')->log('Response: {data}',
                        ['data' => json_encode($resultDispatcherAction['scenario'], JSON_UNESCAPED_SLASHES)]);

                    return $this->getJsonAnswer($resultDispatcherAction['scenario']);
                }
            }
        }

        $firstScenario = $call->firstScenario;

        // Проевряем на черный список
        if ($firstScenario['action'] === self::DO_BLACK_LIST) {

            app()->apiLogger->log('Поиск клиента в черном списке');

            if (ClientPhone::isBlackList($phoneScenario->tenant_id, $clId)) {
                if (!$call->save()) {
                    throw new DbException('error when saving the queue calls');
                }

                $response = [
                    'action' => self::DO_HANGUP,
                    'domain' => $tenantDomain,
                ];

                app()->apiLogger->log('Клиент в черном списке. Разрываю соединение');
                app()->get('apiLogger')->log('Response: {data}',
                    ['data' => json_encode($response, JSON_UNESCAPED_SLASHES)]);

                return $this->getJsonAnswer($response);
            } else {
                app()->apiLogger->log('Клиент не в черном списке');
            }

            // удаляем следующие черные списки
            foreach ($call->scenario as $scenario) {
                if ($scenario['action'] === self::DO_BLACK_LIST) {
                    $call->actNum++;
                    $firstScenario = $call->currentScenario;
                }
            }

            app()->apiLogger->log('Перехожу к следующему сценарию');
        }


        if (!$firstScenario) {
            $response = [
                'action' => self::DO_HANGUP,
                'domain' => $tenantDomain,
            ];

            app()->apiLogger->log('Сценарий не найден. Завершаю звонок');
            app()->get('apiLogger')->log('Response: {data}',
                ['data' => json_encode($response, JSON_UNESCAPED_SLASHES)]);

            return $this->getJsonAnswer($response);
        }

        if ($firstScenario['action'] === self::DO_INFO) {

            app()->apiLogger->log('Сценарий DO_INFO');
            app()->apiLogger->log('Получение данных для сценария');

            $result = PhoneService::getInfoScenario($phoneScenario->tenant_id, $clId);

            if (!empty($result)) {

                app()->apiLogger->log('Данные для сценария найдены');

                if (isset($result['order_id'])) {
                    $call->orderId = $result['order_id'];
                    if (!$call->save()) {
                        throw new DbException('error when saving the queue calls');
                    }
                }
                $result['domain'] = $tenantDomain;

                app()->get('apiLogger')->log('Response: {data}',
                    ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

                return $this->getJsonAnswer($result);
            } else {
                $call->actNum++;
                $firstScenario = $call->currentScenario;

                app()->apiLogger->log('Пустые данные для сценария.');
                app()->apiLogger->log('Перехожу к следующему сценарию');
            }
        }

        // Если ПОСТАНОВКА В ОЧЕРЕДЬ
        if ($firstScenario['action'] === self::DO_QUEUE) {

            app()->apiLogger->log('Сценарий DO_QUEUE');

            $queue = QueueService::dispatcherAction($firstScenario, $call);
            if (isset($queue['scenario'])) {
                $response = $queue['scenario'];
            } else {
                throw new InternalErrorException();
            }
            if (isset($queue['queueId'])) {
                $call->queue = $queue['queueId'];
            }
            //Если разрыв соединения
        } else {
            if ($firstScenario['action'] === self::DO_HANGUP) {

                app()->apiLogger->log('Сценарий DO_HANGUP');

                $response = $firstScenario;
                $call->delete();
            } else {
                $response = $firstScenario;
            }
        }
        if (!$call->save()) {
            throw new DbException('error when saving the queue calls');
        }

        $response['domain'] = $tenantDomain;

        app()->get('apiLogger')->log('Response: {data}', ['data' => json_encode($response, JSON_UNESCAPED_SLASHES)]);

        return $this->getJsonAnswer($response);
    }
}