<?php

namespace api\actions\call;

use api\actions\BaseAction;
use api\exceptions\EmptyParamsException;

class HangupAction extends BaseAction
{

    public function run()
    {

        $callId = $this->params['callid'] ?: null;
        $leg = $this->params['leg'] ?: null;
        $state = $this->params['state'] ?: null;

        if (empty($callId) || empty($leg) || empty($state)) {
//            app()->apiLogger->log('Empty param $callId or $leg or $state');
            throw new EmptyParamsException();
        }

        $response = [
            'state' => 'hangupOk',
        ];

        return $this->getJsonAnswer($response);
    }
}