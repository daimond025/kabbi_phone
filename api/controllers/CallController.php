<?php

namespace api\controllers;

use api\actions\call\IncomingAction;
use api\actions\call\PingAction;
use api\models\order\exceptions\QueueIsNotExistsException;
use api\models\order\OrderUpdateEvent;
use app\actions\ErrorAction;
use app\components\taxiApiRequest\TaxiApiRequest;
use app\models\call\CallSaver;
use app\models\client\Client;
use app\models\client\ClientPhone;
use app\models\order\Order;
use app\models\order\OrderChangeData;
use app\models\phone\PhoneLine;
use app\models\tenant\AutocallFile;
use app\models\tenant\Tenant;
use app\models\tenant\TenantSetting;
use app\models\user\UserDispetcher;
use app\models\worker\TenantHasSms;
use app\models\worker\Worker;
use GuzzleHttp\Exception\GuzzleException;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class CallController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'incoming'         => ['get'],
                    'actcomplete'      => ['get'],
                    'add_dispetcher'   => ['get'],
                    'dispetcher_free'  => ['get'],
                    'hangup'           => ['get'],
                    'notify_client'    => ['get'],
                    'notifyfinished'   => ['get'],
                    'connectagentfail' => ['get'],
                    'asterisk_event'   => ['post'],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error'    => [
                'class' => ErrorAction::className(),
            ],
            'incoming' => [
                'class' => IncomingAction::className(),
            ],
            'ping'     => [
                'class' => PingAction::className(),
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id === 'asterisk_event') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
    ///////////////////////////////МЕТОДЫ АПИ//////////////////////////////////////////////////////

    /**
     * Описание запроса: оповещения сервера о новом входящем вызове.
     * Тип Запроса : GET.
     *
     * @params string callid
     *
     * @params string did
     *
     * @params string clid
     *
     * @params int frominfo 1/0
     *
     * АТС вызывает метод для оповещения сервера о новом входящем вызове.
     *
     * После вызова данного метода app сервер делает следующее:
     *
     * По параметру did ищет в redis сценарий вызова и настройки линии, если в redis данных нет они загружаются из
     *     mysql и кэшируются в re     * is.
     *
     * В redis создается структура звонка по ключу из параметра callid с параметрами did, clid, timeout, queue, actNum
     *     итп (actNum = поря ковый номер действия из сценария).
     *
     * АТС отправляется response содержащий первое действие из сценария вызова, счетчик actNum - инкрементится.
     *
     * Логирует вызов в mysql.
     *
     * Если номер неизвестен - посылается response с doHangup.
     *
     * Пример структуры сценария:
     *
     * {
     *
     *  0: { action: 'doAnswer'},
     *
     *  1: { action: 'doPlay', file: 'digits/3' },
     *
     *  2: { action: 'doMoh', mclass: 'default' }
     *
     * };
     *
     * @params string
     *
     * @return string json
     *
     * {
     *      action: ‘doAction’,
     *      params: ‘actParams’,
     *      domain: '3colors'
     * }
     *
     *  action       params                     назначение
     *
     *  doAnswer     -                          ответить на вызов
     *
     *  doRinging    -                          воспроизводить гудки
     *
     *  doMoh        class                      проирывать музыку
     *
     *  doHangup     -                          разорвать соединение
     *
     * connectAgent  agent, type, timeout       соеденить с оператором
     *                                          agent - sip оператора
     *                                          type - гудки или музыка во время вызова (r/m)
     *                                          timeout - таймаут
     *
     * doPlay        file                       воспроизвести файл если в качестве параметра передан массив -
     *     проигрывается массив
     *
     * doQueue       type, timeout              вызов отправлен в очередь(СМ ниже)
     *
     *                                          doQueue - постановка в очередь или мгновенное распределение.
     *
     *                                          Если в сценарии встретилась такая команда нужно сделать следующее:
     *
     *                                          1)Проверить в redis список свободных операторов для данного номера по
     *     did
     *
     *                                              - если есть забрать от туда оператора и вернуть в ATC response
     *     connectAgent с
     *
     *                                              sipid оператора из redis.
     *
     *                                          2)Если свободные операторы отсутствуют - звонок записать в конец списка
     *     очереди звонков
     *
     *                                              в redis.
     *
     *                                          3)Освободившиеся операторы вызывая специальный метод - устанавливают
     *     свой статус
     *
     *                                              в ожидание, после чего забирают звонки из очереди,
     *
     *                                              если звонков нет - встают в очередь операторов.
     *
     *                                           4)Если освобождается оператор готовый забрать какой-то из вызовов -
     *
     *                                               выполняется вызов метода АТС  (connectagent)
     *
     *
     */
    ///Добавить doBlackList если да то  doHangUp
    //Затем doInfo , если есть заказ и он один то отправить doInfo - возвращает ok,err,cancel,connect-worker
    public function actionIncoming2()
    {
        $params   = $this->getParams($_GET);
        $requset  = new TaxiApiRequest();
        $callid   = isset($params['callid']) ? $params['callid'] : null;
        $did      = isset($params['did']) ? $params['did'] : null;
        $did      = str_replace("+", "", $did);
        $clid     = isset($params['clid']) ? $params['clid'] : null;
        $clid     = str_replace("+", "", $clid);
        $frominfo = isset($params['frominfo']) ? $params['frominfo'] : null;
        if (empty($callid) || empty($did) || empty($clid)) {
            return json_encode("empty_param");
        }
        //Ищем сценарий  вызова в редисе
        $scenario = \Yii::$app->redis_phone_scenario->executeCommand('GET', [$did]);
        $scenario = unserialize($scenario);
        //Если нет  в редисе находим настройки линии в мускуле и пишем в редисе (ключ did)
        if (empty($scenario)) {
            $scenario = PhoneLine::find()
                ->where(["phone" => $did])
                ->asArray()
                ->one();
            // Если нет сценария для данного номера (либо номер не известен), посылаем действие
            // doHangup - разорвать соединение.
            if (empty($scenario)) {
                $result = [
                    'action' => 'doHangup',
                    'domain' => null,
                ];

                return $requset->getJsonAnswer($result);
            }
            $scenarioSerialized = serialize($scenario);
            \Yii::$app->redis_phone_scenario->executeCommand('SET', [$did, $scenarioSerialized]);
        }
        //Получаем ценарий вызова в виде массива
        $incoming_scenario = unserialize($scenario['incoming_scenario']);
        $tenantId          = $scenario['tenant_id'];
        $tenantDomain      = Tenant::getDomain($tenantId);
        $cityId            = $scenario['city_id'];
        // Получаем массив заголовков
        $headersObject = Yii::$app->request->getHeaders();
        $headers       = $headersObject->toArray();

        $incomeIp = isset($headers['x-real-ip'][0]) ? $headers['x-real-ip'][0] : $_SERVER['REMOTE_ADDR'];
        // Получаем с какого хоста был принят вызов
        $pbxPort = Yii::$app->params['pbxPort'];
        $pbxHost = $incomeIp . ':' . $pbxPort;
        $tempArr = explode('_', $callid);
        // Получаем Ид pbx
        $pbxId = $tempArr['0'];

        //Создаем структура звонка
        $callData = [
            'id'       => $callid,
            'did'      => $did,
            'clid'     => $clid,
            'scenario' => $incoming_scenario,
            'queue'    => null,
            'actNum'   => 0,
            'pbxHost'  => $pbxHost,
            'pbxId'    => $pbxId,
            'tenantId' => $tenantId,
            'cityId'   => $cityId,
        ];

        if (is_array($incoming_scenario)) {
            $firstScenario = $incoming_scenario[0];
        } else {
            $firstScenario = $incoming_scenario;
        }

        if ($frominfo == 1) {
            $callData = serialize($callData);
            \Yii::$app->redis_active_call->executeCommand('SET', [$callid, $callData]);
            $callData = \Yii::$app->redis_active_call->executeCommand('GET', [$callid]);
            $callData = unserialize($callData);
            $scenario = $callData['scenario'];
            foreach ($scenario as $sc) {
                $currrentScenario = $sc;
                if ($currrentScenario['action'] == "doQueue") {
                    $resultQueue = $this->queueAction($currrentScenario, $callData);
                    if (isset($resultQueue['scenario'])) {
                        $result = $resultQueue['scenario'];
                    }
                    if (isset($resultQueue['queueId'])) {
                        $callData['queue'] = $resultQueue['queueId'];
                    }
                    // Увеличиваем счетчик действий
                    $callData['actNum'] = array_search($sc, $scenario);
                    $callData           = serialize($callData);
                    \Yii::$app->redis_active_call->executeCommand('SET', [$callid, $callData]);
                    $result["domain"] = $tenantDomain;

                    return $requset->getJsonAnswer($result);
                }
            }
        }
        //Проверка на черный список
        //Если клиент в черном списке заказчика то отвечаем -doHangup -разрыв соединения
        //Иначе отдаем следующий сценарий
        if (ClientPhone::isBlackList($tenantId, $clid)) {
            $result   = [
                'action' => 'doHangup',
                'domain' => $tenantDomain,
            ];
            $callData = serialize($callData);
            \Yii::$app->redis_active_call->executeCommand('SET', [$callid, $callData]);

            return $requset->getJsonAnswer($result);
        } else {
            $callData['actNum'] = $callData['actNum'] + 1;
            $actNum             = $callData['actNum'];
            if (isset($incoming_scenario[$actNum])) {
                $firstScenario = $incoming_scenario[$actNum];
            }
        }
        if ($firstScenario['action'] == 'doBlackList') {
        }

        if ($firstScenario['action'] == 'doMoh') {
            $result = [
                'action' => 'doMoh',
                'mclass' => $firstScenario['mclass'],
            ];
        }

        //Если автоинформатор и есть текущий заказ то отдаем сценарий отзвона
        if ($firstScenario['action'] == 'doInfo') {
            $result = $this->getInfoScenarioByPhone($tenantId, $clid);
            if ($result) {
                if (isset($result["order_id"])) {
                    $callData["orderId"] = $result["order_id"];
                }
                $callData = serialize($callData);
                \Yii::$app->redis_active_call->executeCommand('SET', [$callid, $callData]);
                $result["domain"] = $tenantDomain;

                return $requset->getJsonAnswer($result);
            } else {
                $callData['actNum'] = $callData['actNum'] + 1;
                $actNum             = $callData['actNum'];
                if (isset($incoming_scenario[$actNum])) {
                    $firstScenario = $incoming_scenario[$actNum];
                }
            }
        }

        // Если ПОСТАНОВКА В ОЧЕРЕДЬ
        if ($firstScenario['action'] == 'doQueue') {
            $resultQueue = $this->queueAction($firstScenario, $callData);
            if (isset($resultQueue['scenario'])) {
                $result = $resultQueue['scenario'];
            }
            if (isset($resultQueue['queueId'])) {
                $callData['queue'] = $resultQueue['queueId'];
            }
            //Если разрыв соединения
        } else {
            if ($firstScenario['action'] == 'doHangup') {
                $result = $firstScenario;
                $this->deleteCall($callData);
            } else {
                $result = $firstScenario;
            }
        }
        $callData = serialize($callData);
        \Yii::$app->redis_active_call->executeCommand('SET', [$callid, $callData]);
        $result["domain"] = $tenantDomain;

        return $requset->getJsonAnswer($result);
    }

    /**
     * Вызывается в случае не успешного соединения с оператором.
     *
     * GET http://name:port/call/connectagentfail
     *
     * Параметры:
     * param                     value
     *
     * callid                    уникальный идентификатор вызова формат $pbxid_$callid,
     *                              где $pbxid - идентификатор АТС,
     *                              $callid - уникальный id вызова (уникален только внутри АТС)
     *
     * agent                     sipid оператора
     *
     * state                     статус действия:
     *                              timeout - оператор не взял трубку
     *                              congestion - оператор отбил не взял трубкуов
     *                              chanunavail - оператор недоступен
     *                              cancel - пользователь бросил трубу
     *
     *  При статусах timeout, congestion, chanunavail - требуется:
     *  вернуть вызов в топ очереди
     *          или
     *  выдать другого оператора(если есть свободный).
     *
     * При статусе cancel - оператора вернуть в топ очереди.
     *
     * Ответы: application/json
     * { action: ‘doAction’, params: ‘actParams’ }
     * Фактически может вернуть либо
     * doQueue (если нет других свободных операторов)
     * или
     * connectAgent (если свободные операторы есть)
     *
     * На cancel ответ { action: ‘callCancel’, state: ‘ok’ } ну или err если что не так
     */
    public function actionConnectagentfail()
    {
        $params             = $this->getParams($_GET);
        $requset            = new TaxiApiRequest();
        $callid             = isset($params['callid']) ? $params['callid'] : null;
        $agent              = isset($params['agent']) ? $params['agent'] : null;
        $state              = isset($params['state']) ? $params['state'] : null;
        $searchAgentActions = ["timeout", "congestion", "chanunavail"];

        app()->get('apiLogger')->setCallId($callid);
        app()->get('apiLogger')->log('Ошибка соединения с оператором {agent}', ['agent' => $agent]);

        switch ($state) {
            case 'timeout':
                app()->get('apiLogger')->log('Оператор {agent} не взял трубку', ['agent' => $agent]);
                break;
            case 'congestion':
                app()->get('apiLogger')->log('Оператор {agent} отбил не взял трубку', ['agent' => $agent]);
                break;
            case 'chanunavail':
                app()->get('apiLogger')->log('Оператор {agent} недоступен', ['agent' => $agent]);
                break;
            case 'cancel':
                app()->get('apiLogger')->log('Клиент {callid} бросил трубку', ['callid' => $callid]);
                break;
        }

        if (isset($callid) && isset($agent) && isset($state)) {

            if (in_array($state, $searchAgentActions)) {
                $callData = \Yii::$app->redis_active_call->executeCommand('GET', [$callid]);
                $callData = unserialize($callData);
                if (!empty($callData)) {

                    app()->apiLogger->log('Ставлю оператора {operator} на паузу', ['operator' => $agent]);

                    //$this->addMessageToDispatcher($agent, "set_pause", $state);
                    $dispetcherQueueId = $callData['tenantId'];
                    $callCityId        = $callData['cityId'];

                    $dispetcherQueueLen = \Yii::$app->redis_free_dispetcher_queue->executeCommand('LLEN',
                        [$dispetcherQueueId]);

                    app()->get('apiLogger')->log('Ищу свободных операторов. Найдено {count}',
                        ['count' => $dispetcherQueueLen]);

                    //Если есть свободные операторы то перебираем их,
                    if ($dispetcherQueueLen > 0) {
                        for ($i = 0; $i < $dispetcherQueueLen; $i++) {
                            $dispetcherId   = \Yii::$app->redis_free_dispetcher_queue->executeCommand('LINDEX',
                                [$dispetcherQueueId, $i]);
                            $dispetcherData = \Yii::$app->redis_active_dispetcher->executeCommand('GET',
                                [$dispetcherId]);
                            $dispetcherData = unserialize($dispetcherData);
                            $cityArr        = (array)$dispetcherData['city_arr'];
                            foreach ($cityArr as $city => $cityId) {
                                if ($callCityId == $cityId) {
                                    //Удаляем дичпетчера из очереди
                                    \Yii::$app->redis_free_dispetcher_queue->executeCommand('LSET',
                                        [$dispetcherQueueId, $i, 'null']);
                                    \Yii::$app->redis_free_dispetcher_queue->executeCommand('LREM',
                                        [$dispetcherQueueId, 0, 'null']);
                                    // Отвечаем соединить с оператором
                                    app()->apiLogger->log('Соединяю с диспетчером {agent}',
                                        ['agent' => $dispetcherData['sip_id']]);
                                    $result = [
                                        'action'  => 'connectAgent',
                                        'agent'   => $dispetcherData['sip_id'],
                                        'type'    => 'r',
                                        'timeout' => '10',
                                    ];

                                    app()->get('apiLogger')->log('Перенаправляю звонок на оператора {operator}',
                                        ['operator' => $dispetcherData['sip_id']]);
                                    app()->get('apiLogger')->log('Response: {data}',
                                        ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

                                    return $requset->getJsonAnswer($result);
                                }
                            }
                        }
                    }
                    //Если нет свободных операторов добавляем звонок в очередь звонков
                    $callQueueId = $callData['tenantId'] . '_' . $callData['cityId'];
                    Yii::$app->redis_queue_call->executeCommand('lpush', [$callQueueId, $callid]);
                    $callData['queue'] = $callQueueId;
                    $callData          = serialize($callData);
                    \Yii::$app->redis_active_call->executeCommand('SET', [$callid, $callData]);
                    $result = [
                        'action'  => 'doQueue',
                        'type'    => 'r',
                        'timeout' => '10',
                    ];

                    app()->get('apiLogger')->log('Ставлю звонок в очередь');
                    app()->get('apiLogger')->log('Response: {data}',
                        ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

                    return $requset->getJsonAnswer($result);
                }
            } else {
                if ($state == "cancel") {
                    $callData = \Yii::$app->redis_active_call->executeCommand('GET', [$callid]);
                    $callData = unserialize($callData);
                    //Удалим звонок отовсюду
                    if (!empty($callData)) {
                        $this->deleteCall($callData);
                    }

                    //Освобождаем оператора и посылаемему звонок если есть
                    /** @var UserDispetcher $operator */
                    $operator = UserDispetcher::find()
                        ->where([
                            'sip_id' => $agent,
                        ])
                        ->one();

                    if ($operator) {
                        $dispetcherData = \Yii::$app->redis_active_dispetcher->executeCommand('GET',
                            [$operator->user_id]);
                        app()->get('apiLogger')->log('Освобождаю оператора {operator}', ['operator' => $agent]);
                        app()->get('apiLogger')->log('Ищу звонки в очереди');
                        $dispetcherData = unserialize($dispetcherData);
                    }
                    if (!empty($dispetcherData)) {
                        $dispetcherQueueId = $dispetcherData['tenant_id'];
                        $sipId             = $dispetcherData['sip_id'];
                        //если диспетчер готов принимать заказы,  то найти для него звонок и отдать атс,
                        //если звонка нет, добавить в очередь свободных операторов
                        //Найти звонок для диспетчера
                        $tenantId = $dispetcherData['tenant_id'];
                        $cityArr  = (array)$dispetcherData['city_arr'];
                        foreach ($cityArr as $city => $cityId) {
                            $callQueueId = $tenantId . '_' . $cityId;
                            $callId      = \Yii::$app->redis_queue_call->executeCommand('LINDEX', [$callQueueId, 0]);
                            if (!empty($callId)) {
                                // Получить ip:port для соедиения с атс
                                $callData = \Yii::$app->redis_active_call->executeCommand('GET', [$callId]);
                                if (!empty($callData)) {
                                    $callData   = unserialize($callData);
                                    $pbxHost    = $callData['pbxHost'];
                                    $timeout    = 10;
                                    $type       = 'r';
                                    $sendResult = $this->sendCallToDispetcher($pbxHost, $callId, $sipId, $timeout,
                                        $type);
                                    if (isset($sendResult['state'])) {
                                        if ($sendResult['state'] == 'ok') {
                                            // удаляем звонок из очереди
                                            \Yii::$app->redis_queue_call->executeCommand('LPOP', [$callQueueId]);
                                            //возвращаем результат
                                            $result = [
                                                'action' => 'callCancel',
                                                'state'  => 'ok',
                                            ];

                                            app()->get('apiLogger')->log('Удаляю звонок из очереди');
                                            app()->get('apiLogger')->log('Response: {data}',
                                                ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

                                            return $requset->getJsonAnswer($result);
                                        } else {
                                            if ($sendResult['state'] == "restidnotfound") {
                                                $this->deleteCall($callData);
                                                //возвращаем результат
                                                $result = [
                                                    'action' => 'callCancel',
                                                    'state'  => 'ok',
                                                ];

                                                app()->get('apiLogger')->log('Response: {data}',
                                                    ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

                                                return $requset->getJsonAnswer($result);
                                            }
                                        }
                                    }
                                } else {
                                    // удаляем звонок из очереди
                                    \Yii::$app->redis_queue_call->executeCommand('LPOP', [$callQueueId]);
                                }
                            }
                        }
                        //добавить в очередь свободных операторов если его там нет
                        $dispetcherQueueLen = \Yii::$app->redis_free_dispetcher_queue->executeCommand('LLEN',
                            [$dispetcherQueueId]);

                        app()->get('apiLogger')->log('Ставлю оператора {operator} в очередь', ['operator' => $agent]);

                        $isIn = 0;
                        if ($dispetcherQueueLen != 0) {
                            // Обходим очередь диспетчеров
                            for ($i = 0; $i < $dispetcherQueueLen; $i++) {
                                // получаем ид диспетчера из очереди
                                $id = \Yii::$app->redis_free_dispetcher_queue->executeCommand('LINDEX',
                                    [$dispetcherQueueId, $i]);
                                if ($agent == $id) {
                                    $isIn = 1;
                                }
                            }
                        }
                        if ($isIn == 0) {
                            \Yii::$app->redis_free_dispetcher_queue->executeCommand('rpush',
                                [$dispetcherQueueId, $agent]);
                        }
                    } else {
                        $result = [
                            'err',
                        ];

                        app()->get('apiLogger')->log('Response: {data}',
                            ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

                        return $requset->getJsonAnswer($result);
                    }
                }
            }
        }
    }

    /**
     * Описание запроса: оповещения сервера о том что действие выполнено.
     * Тип Запроса : GET.
     *
     * После вызова данного метода app сервер делает следующее:
     *
     * 1)Находит в redis звонок по callid.
     *
     * 2)Находит сценарий во did
     *
     * 3)Возвращает response со следующим действием из сценария и увеличивает счетчик сценария.
     *
     * @params string callid - ид звонка
     *
     *
     * @params string state (ok/err)
     *
     * @return string json
     *
     * {
     *      action: ‘doAction’,
     *      params: ‘actParams’
     * }
     */
    public function actionActcomplete()
    {
        $params  = $this->getParams($_GET);
        $requset = new TaxiApiRequest();
        $callid  = isset($params['callid']) ? $params['callid'] : null;
        $state   = isset($params['state']) ? $params['state'] : null;

        app()->get('apiLogger')->setCallId($callid);
        app()->get('apiLogger')->log('Результат выполнения сценария');

        if (empty($callid) || empty($state)) {
            app()->get('apiLogger')->log('Не указан(ы) параметр(ы) $orderId и/или $statusId');

            return json_encode("empty_param");
        }
        $callData = \Yii::$app->redis_active_call->executeCommand('GET', [$callid]);
        $callData = unserialize($callData);
        $scenario = $callData['scenario'];
        $orderId  = $callData['orderId'];
        $actNum   = $callData['actNum'];
        $tenantId = $callData['tenantId'];
        $clid     = $callData['clid'];
        $clid     = str_replace("+", "", $clid);
        //Если последний сценарий был автоинофрматором, то проверяем его ответ и логируем
        if ($scenario[$actNum]) {
            $lastScenario = $scenario[$actNum];
            if ($lastScenario['action'] == 'doInfo') {
                if (isset($orderId)) {
                    $tenantId  = $callData['tenantId'];
                    $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);
                    $orderData = unserialize($orderData);
                    if ($orderData) {
                        switch ($state) {
                            case "cancel":
                                $this->cancelOrder($tenantId, $orderId, $orderData);
                                break;
                            case "ok":
                                $this->okNotify($tenantId, $orderId);
                                break;
                            case "connectdriver":
                                $this->connectDriver($tenantId, $orderId);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        if (isset($scenario[$actNum - 1])) {
            $prevScenario = $scenario[$actNum - 1];

            switch ($prevScenario['action']) {

                // Если предыдущий сценарий был doQueue
                case 'doQueue':
                    $queueId = $tenantId . '_' . $callData['cityId'];

                    \Yii::$app->redis_queue_call->executeCommand('LREM', [$queueId, 0, $callid]);

                    app()->get('apiLogger')->log('Предыдущий сценарий: {scenario}',
                        ['scenario' => $prevScenario['action']]);
                    app()->get('apiLogger')->log('Удаляю звонок {callId} из очереди {queueId}',
                        ['callId' => $callid, 'queueId' => $queueId]);

            }
        }

        if (isset($scenario[$actNum])) {
            $currentScenario = $scenario[$actNum];
            $result          = [];

            app()->get('apiLogger')->log('Текущий сценарий: {scenario}', ['scenario' => $currentScenario['action']]);

            if ($currentScenario['action'] == 'doInfo') {

                app()->get('apiLogger')->log('Получение данных для сценария');

                $result = $this->getInfoScenarioByPhone($tenantId, $clid);
                if ($result) {
                    if (isset($result["order_id"])) {
                        $callData["orderId"] = $result["order_id"];
                    }
                    $callData['actNum'] = $actNum + 1;
                    $callData           = serialize($callData);
                    \Yii::$app->redis_active_call->executeCommand('SET', [$callid, $callData]);

                    app()->get('apiLogger')->log('Данные получены');
                    app()->get('apiLogger')->log('Response: {data}',
                        ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

                    return $requset->getJsonAnswer($result);
                } else {

                    app()->get('apiLogger')->log('Пустые данные');

                    if (isset($scenario[$actNum + 1])) {
                        $currentScenario = $scenario[$actNum + 1];
                        $actNum          = $actNum + 1;

                        app()->get('apiLogger')->log('Следующий сценарий: {scenario}',
                            ['scenario' => $currentScenario['action']]);
                    }
                }
            }

            if ($currentScenario['action'] == 'doRedirect') {

                $isPhoneLine = PhoneLine::find()->where([
                    'tenant_id' => $tenantId,
                    'block'     => 0,
                    'phone'     => $currentScenario['phline'],
                ])->exists();

                if ($isPhoneLine) {
                    $domain = Tenant::getDomain($tenantId);
                    $result = [
                        'action'    => $currentScenario['action'],
                        'domain'    => $domain,
                        'phline'    => $currentScenario['phline'],
                        'phone'     => $currentScenario['phone'],
                        'typemusic' => $currentScenario['typemusic'],
                        'timeout'   => $currentScenario['timeout'],
                    ];

                    if ($currentScenario['typemusic'] === 'm') {
                        $result['classmusic'] = !empty($currentScenario['classmusic']) ? $currentScenario['classmusic'] : $domain;
                    }

                    $callData['actNum'] = $actNum + 1;
                    $callData           = serialize($callData);
                    \Yii::$app->redis_active_call->executeCommand('SET', [$callid, $callData]);


                    app()->get('apiLogger')->log('Response: {data}',
                        ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

                    return $requset->getJsonAnswer($result);
                } else {

                    app()->get('apiLogger')->log('Телефонная линия {phone} не найдена',
                        ['phone' => $currentScenario['phline']]);

                    if (isset($scenario[$actNum + 1])) {
                        $currentScenario = $scenario[$actNum + 1];
                        $actNum          = $actNum + 1;

                        app()->get('apiLogger')->log('Следующий сценарий: {scenario}',
                            ['scenario' => $currentScenario['action']]);
                    }
                }
            }

            // Если ПОСТАНОВКА В ОЧЕРЕДЬ
            if ($currentScenario['action'] == 'doQueue') {
                $resultQueue = $this->queueAction($currentScenario, $callData);
                if (isset($resultQueue['scenario'])) {
                    $result = $resultQueue['scenario'];
                }
                if (isset($resultQueue['queueId'])) {
                    $callData['queue'] = $resultQueue['queueId'];
                }
                //Если разрыв соединения
            } else {
                if ($currentScenario['action'] == 'doHangup') {
                    $result = $currentScenario;
                    $this->deleteCall($callData);
                } else {
                    $result = $currentScenario;
                }
            }
            // Увеличиваем счетчик действий
            $callData['actNum'] = $actNum + 1;
            $callData           = serialize($callData);
            if ($currentScenario['action'] != 'doHangup') {
                \Yii::$app->redis_active_call->executeCommand('SET', [$callid, $callData]);
            }

            app()->get('apiLogger')->log('Response: {data}', ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

            return $requset->getJsonAnswer($result);
        }

        $result = ['action' => 'doHangup', 'domain' => Tenant::getDomain($tenantId)];

        app()->get('apiLogger')->log('Response: {data}', ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

        return $requset->getJsonAnswer($result);
    }

    /**
     * Описание запроса: Разрыв соединения.
     * Тип Запроса : GET.
     *
     * После вызова данного метода app сервер делает следующее:
     *
     * 1)Удаляет звонок из redis.
     *
     * 2)Логирует в mysql.
     *
     * @params string callid - ид звонка
     *
     * @params string leg  сторона разорвавшая вызов - pbx - атс, agent - оператор, caller - клиент, api - Апи
     *
     * @params string state  в каком состоянии пришел разрыв   init - до соединения с оператором
     *                                                         connect - во время соединения с оператором
     *                                                         complete - во время разговора
     *
     *
     * @return string json
     *
     * {
     *      state: %state%,
     *
     * }
     * state ->
     *          hangupOk - успешно обработан и удален из хранилища
     *          err - ошибка
     */
    public function actionHangup()
    {
        $params  = $this->getParams($_GET);
        $requset = new TaxiApiRequest();
        $callid  = isset($params['callid']) ? $params['callid'] : null;
        $leg     = isset($params['leg']) ? $params['leg'] : null;
        $state   = isset($params['state']) ? $params['state'] : null;

        app()->get('apiLogger')->setCallId($callid);
        app()->get('apiLogger')->log('Разрыв соединения');

        if (empty($callid) || empty($leg) || empty($state)) {
            app()->get('apiLogger')->log('Не указан(ы) параметр(ы) $callid, $leg и/или $state');

            return json_encode("empty_param");
        }
        $callData = \Yii::$app->redis_active_call->executeCommand('GET', [$callid]);
        $callData = unserialize($callData);
        $this->deleteCall($callData);
        $result = [
            'state' => 'hangupOk',
        ];

        app()->get('apiLogger')->log('Response: {data}', ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

        return $requset->getJsonAnswer($result);
    }

    /**
     * Описание запроса: установить статус оператора.
     * Тип Запроса : GET.
     *
     * @params string agent - id оператора
     *
     * @params string state  в каком состоянии пришел разрыв   статус
     *                                                                  idle - ожидает вызова
     *                                                                  unavailable - недоступен
     *
     *
     * @return string json
     *
     * {
     *      state: %state%,
     *
     * }
     * state ->
     *          ok           - статус успешно установлен
     *          unknownAgent - неизвестный агент
     *          unknownState - неизвестный статус
     */
    public function actionSetstate()
    {
        $params  = $this->getParams($_GET);
        $requset = new TaxiApiRequest();
        $agent   = isset($params['agent']) ? $params['agent'] : null;


        $state = isset($params['state']) ? $params['state'] : null;
        if (empty($agent) || empty($state)) {

            app()->get('apiLogger')->log('Не указан(ы) параметр(ы) $agent и/или $state');

            return json_encode("empty_param");
        }
        if ($state != "idle" && $state != "unavailable") {
            $result = [
                'state' => 'unknownState',
            ];

            app()->get('apiLogger')->log('Неизвестный статус');
            app()->get('apiLogger')->log('Response: {data}', ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

            return $requset->getJsonAnswer($result);
        }

        $dispetcherData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$agent]);
        $dispetcherData = unserialize($dispetcherData);

        if (empty($dispetcherData)) {
            $result = [
                'state' => 'unknownAgent',
            ];

            app()->get('apiLogger')->log('Оператор {operator} не найден в списке активных операторов',
                ['operator' => $agent]);
            app()->get('apiLogger')->log('Response: {data}', ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

            return $requset->getJsonAnswer($result);
        }

        $dispetcherQueueId = $dispetcherData['tenant_id'];
        $sipId             = $dispetcherData['sip_id'];
        //если диспетчер готов принимать заказы,  то найти для него звонок и отдать атс,
        //если звонка нет, добавить в очередь свободных операторов
        if ($state == "idle") {
            //Найти звонок для диспетчера
            $tenantId = $dispetcherData['tenant_id'];
            $cityArr  = (array)$dispetcherData['city_arr'];

            app()->get('apiLogger')->log('Оператор {agent} работает в филиалах {cityIds}',
                ['agent' => $sipId, 'cityIds' => implode(', ', $dispetcherData['city_arr'])]);
            app()->get('apiLogger')->log('Ищу звонок для оператора {agent}', ['agent' => $sipId]);

            foreach ($cityArr as $city => $cityId) {

                app()->get('apiLogger')->log('Ищу звонок в очереди в филиале {cityId}', ['cityId' => $cityId]);

                $callQueueId = $tenantId . '_' . $cityId;
                $callId      = \Yii::$app->redis_queue_call->executeCommand('LINDEX', [$callQueueId, 0]);
                while (!empty($callId)) {
                    $callData = \Yii::$app->redis_active_call->executeCommand('GET', [$callId]);
                    if (!empty($callData)) {

                        app()->get('apiLogger')->log('Найден звонок {callId}', ['callId' => $callId]);

                        $callData   = unserialize($callData);
                        $pbxHost    = $callData['pbxHost'];
                        $timeout    = 10;
                        $type       = 'r';
                        $sendResult = $this->sendCallToDispetcher($pbxHost, $callId, $sipId, $timeout, $type);
                        if (isset($sendResult['state'])) {
                            if ($sendResult['state'] == 'ok') {
                                // удаляем звонок из очереди
                                \Yii::$app->redis_queue_call->executeCommand('LPOP', [$callQueueId]);
                                //возвращаем результат
                                $result = [
                                    'state' => 'ok',
                                ];

                                app()->get('apiLogger')->log('Удаляю звонок {callId} из очереди',
                                    ['callId' => $callId]);
                                app()->get('apiLogger')->log('Response: {data}',
                                    ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

                                return $requset->getJsonAnswer($result);
                            } elseif ($sendResult['state'] == 'restidnotfound') {
                                $this->deleteCall($callData);
                            }
                        }
                    }
                    // удаляем звонок из очереди
                    \Yii::$app->redis_queue_call->executeCommand('LPOP', [$callQueueId]);

                    $callId = \Yii::$app->redis_queue_call->executeCommand('LINDEX', [$callQueueId, 0]);
                }

            }

            app()->get('apiLogger')->log('Нет звонков в очереди');
            app()->get('apiLogger')->log('Ищу оператора {agent} в очереди', ['agent' => $sipId]);

            //добавить в очередь свободных операторов если его там нет
            $dispetcherQueueLen = \Yii::$app->redis_free_dispetcher_queue->executeCommand('LLEN', [$dispetcherQueueId]);
            $isIn               = 0;
            if ($dispetcherQueueLen != 0) {
                // Обходим очередь диспетчеров
                for ($i = 0; $i < $dispetcherQueueLen; $i++) {
                    // получаем ид диспетчера из очереди
                    $id = \Yii::$app->redis_free_dispetcher_queue->executeCommand('LINDEX', [$dispetcherQueueId, $i]);
                    if ($agent == $id) {
                        $isIn = 1;
                    }
                }
            }
            if ($isIn == 0) {
                \Yii::$app->redis_free_dispetcher_queue->executeCommand('rpush', [$dispetcherQueueId, $agent]);

                app()->get('apiLogger')->log('Оператор {operator} не в очереди. Добавляю его туда',
                    ['operator' => $agent]);

            } else {

                app()->get('apiLogger')->log('Оператор {operator} в очереди.', ['operator' => $agent]);

            }
        }

        // Если нужно установить статус недоступен- значит нужно удалить из очереди свободных диспетчеров.
        if ($state == "unavailable") {
            //Длина очереди диспетчеров
            $dispetcherQueueLen = \Yii::$app->redis_free_dispetcher_queue->executeCommand('LLEN', [$dispetcherQueueId]);

            app()->get('apiLogger')->log('Ищу операторов в очереди. Найдено {count}.',
                ['count' => $dispetcherQueueLen]);

            if ($dispetcherQueueLen != 0) {
                // Обходим очередь диспетчеров
                for ($i = 0; $i < $dispetcherQueueLen; $i++) {
                    // получаем ид диспетчера из очереди
                    $id = \Yii::$app->redis_free_dispetcher_queue->executeCommand('LINDEX', [$dispetcherQueueId, $i]);
                    if ($agent == $id) {
                        //Удаляем диспетчеров из очереди
                        \Yii::$app->redis_free_dispetcher_queue->executeCommand('LSET',
                            [$dispetcherQueueId, $i, 'null']);
                        \Yii::$app->redis_free_dispetcher_queue->executeCommand('LREM',
                            [$dispetcherQueueId, 0, 'null']);

                        app()->get('apiLogger')->log('Найден оператор {operator} в очереди. Удаляю его',
                            ['operator' => $id]);

                    } else {

                        app()->get('apiLogger')->log('Найден оператор {operator} в очереди.', ['operator' => $id]);

                    }
                }
            }
        }
        $result = [
            'state' => 'ok',
        ];

        app()->get('apiLogger')->log('Response: {data}', ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

        return $requset->getJsonAnswer($result);
    }

    public function actionGet_notify_data()
    {
        $params        = $this->getParams($_GET);
        $operatorPhone = ArrayHelper::getValue($params, 'did');
        $clientPhone   = ArrayHelper::getValue($params, 'clid');

        if (empty($operatorPhone) || empty($clientPhone)) {
            return ' empty client phone or operator phone';
        }

        $phoneLine = PhoneLine::getLineDataByPhone($operatorPhone);
        if (empty($phoneLine)) {
            return 'empty phone line';
        }

        $notifyData = $this->getInfoScenarioByPhone($phoneLine->tenant_id, $clientPhone);
        if (!$notifyData) {
            return 'empty notify data';
        }

        return 'scriptname=GootaxCallIVR&startparam1={"callparams": [' . json_encode($notifyData) . ']}';
    }


    public function getCompanyData($callsign ){

        if(is_null($callsign)){
            return null;
        }
        $workerData = Worker::find()
            ->where([
                'callsign'     => $callsign,
            ])
            ->one();
        if(!$workerData) return null;

        $companyData = $workerData->tenantCompany;
        return [
            'worker' => $workerData,
            'company' => $companyData
        ];
    }
    public function getApiMessageBird(){
        $message_dird = TenantHasSms::find()
            ->where([
                'default' => 1
            ])
            ->asArray()
            ->one();

        return $message_dird;
    }
    public function getNotifyDataAutoCall($tenantId, $orderId, $phone, $statusId)
    {
        $domain    = Tenant::getDomain($tenantId);
        $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);

        $orderData = unserialize($orderData);
        if (empty($orderData)) {
            $orderData = Order::findOrderFromMysql($orderId);
        }
        if (empty($orderData) && (empty($orderData['worker']['callsign']))) {
            return null;
        }
        return $orderData['worker']['callsign'];

    }
    public function getPrase($order_id){

        $order = Order::find()
            ->where([
                'order_id'     => $order_id,
            ])
            ->one();

        $number_order = $order->order_number;
        $address_serail = unserialize($order->address);

        $str = 'Sie haben den Auftrag Nummer ' .$number_order . ' erhalten.' ;
        if(isset($address_serail['A']['street'])){
            $str .= ' Die Abholadresse lautet ' .  $address_serail['A']['street']. ' .';
        }
        return $str;

    }

    public function actionNotify_client(){

        $params   = $this->getParams($_GET);
        $tenantId = isset($params['tenant_id']) ? (int)$params['tenant_id'] : null;
        $orderId  = isset($params['order_id']) ? $params['order_id'] : null;
        $phone    = isset($params['phone']) ? $params['phone'] : null;
        $statusId = isset($params['status_id']) ? $params['status_id'] : null;

        $work_callsign   = $this->getNotifyDataAutoCall($tenantId, $orderId, $phone, $statusId);
        $worker_company = $this->getCompanyData($work_callsign);
        $worker = $worker_company['worker'];
        $company= $worker_company['company'];
        $response = [
            'result' => 0,
        ];

        if($work_callsign || (!is_null($worker)) ){

            $dateprovider = $this->getApiMessageBird();
            $prase =  $this->getPrase($orderId);

           /// $worker->phone = '79772602817';


            $MessageBird = new \MessageBird\Client($dateprovider['password']);
            $VoiceMessage             = new \MessageBird\Objects\VoiceMessage();
            $VoiceMessage->recipients = array ($worker->phone);
            $VoiceMessage->originator =$company->phone;
            $VoiceMessage->body = $prase;
            $VoiceMessage->language = 'de-de';
            $VoiceMessage->voice = 'female';
            $VoiceMessage->ifMachine = 'continue';
            try {
                $VoiceMessageResult = $MessageBird->voicemessages->create($VoiceMessage);
                Yii::warning('025');
                $response = [
                    'result' => 1,
                ];
            } catch (\Exception $e) {
                Yii::error($e);
                $response = [
                    'result' => 0,
                ];
            }
        }
        return json_encode($response);
    }



    public function actionNotify_client_old()
    {

        $params   = $this->getParams($_GET);
        $tenantId = isset($params['tenant_id']) ? (int)$params['tenant_id'] : null;
        $orderId  = isset($params['order_id']) ? $params['order_id'] : null;
        $phone    = isset($params['phone']) ? $params['phone'] : null;
        $statusId = isset($params['status_id']) ? $params['status_id'] : null;
        $params   = $this->getNotifyData($tenantId, $orderId, $phone, $statusId);


        app()->get('apiLogger')->log('Уведомление клиента');
        if ($params) {

            var_dump($_POST);
            var_dump($_GET);
            exit();
            // Костыль
            if (ArrayHelper::isIn($tenantId, [3083, 2083, 4148], true)) {
                return json_encode($this->externalNotifyClient($params));
            }

            $url = $this->getNotifySereverUrl();

            app()->get('apiLogger')->log('Запрос на сервер {url}', ['url' => $url]);
            app()->get('apiLogger')->log('Params: {data}', ['data' => json_encode($params, JSON_UNESCAPED_SLASHES)]);

            try {
                $client = new \GuzzleHttp\Client();

                $options   = [
                    'form_params' => $params,
                    'headers'     => [
                        'Request-Id' => app()->get('apiLogger')->getId(),
                    ],
                    'timeout'     => 5,
                ];
                $startTime = microtime(true);
                $response  = $client->request('POST', $url, $options);
                $result    = json_decode($response->getBody(), true);
                $timeout   = microtime(true) - $startTime;

                app()->get('apiLogger')->log('Запрос на сервер занял {ms} секунд', ['ms' => round($timeout, 4)]);
                app()->get('apiLogger')->log('Response: {data}',
                    ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);


                if (isset($result[0]['state'])) {
                    if ($result[0]['state'] == 'ok') {
                        $response = [
                            'result' => 1,
                        ];
                        $this->logCallNotify($tenantId, $orderId);

                        app()->get('apiLogger')->log('Сохранение в OrderChangeData');
                        app()->get('apiLogger')->log('Response: {data}',
                            ['data' => json_encode($response, JSON_UNESCAPED_SLASHES)]);

                        return json_encode($response);
                    }
                }

            } catch (\Exception $ex) {
                app()->get('apiLogger')->log('Неудачный запрос. {ex}', ['ex' => get_class($ex), 'url' => $url]);
                Yii::error($ex);
            }

        } else {
            app()->get('apiLogger')->log('Пустые данные notifyData');
        }
        $response = [
            'result' => 0,
        ];

        app()->get('apiLogger')->log('Response: {data}', ['data' => json_encode($response, JSON_UNESCAPED_SLASHES)]);

        return json_encode($response);
    }

    /**
     * Получение ивентов от Астериска и логирование их в базу
     * @return int 1/0
     */
    public function actionAsterisk_event()
    {
        $eventData = Yii::$app->request->rawBody;
        $eventData = json_decode($eventData, true);
        //Ловим Cdr - окончание вызова, отправляем на обработку
        //и складываем в мускул
        if (is_array($eventData)) {
            // Эта запись не важна для логирования(дубль)
            if ($eventData['event'] == "Cdr") {
                if ($eventData['duration'] == 0 && $eventData['disposition'] == "NO ANSWER") {
                    return 0;
                }
                $callData       = $eventData;
                $newCall        = new CallSaver();
                $callSaveResult = $newCall->saveCallToBase($callData);
                if ($callSaveResult) {
                    return 1;
                }
                //                Yii::error("Ошибка сохранения звонка в базу. Информация о звонке:");
                //                Yii::error($callData);

                return 0;
            }
        }

        return 0;
    }

    public function actionNotifyfinished()
    {
        $params   = $this->getParams($_GET);
        $orderId  = isset($params['order_id']) ? $params['order_id'] : null;
        $statusId = isset($params['status_id']) ? $params['status_id'] : null;
        $domain   = isset($params['domain']) ? $params['domain'] : null;
        $result   = isset($params['result']) ? $params['result'] : null;
        $tenantId = null;

        app()->get('apiLogger')->log('Завершение уведомления клиента');

        if (!empty($domain)) {
            $tenantId = Tenant::getIdbyDomain($domain);
        }

        app()->get('apiLogger')->log('Ищу заказ в Redis\'е');

        $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);
        $orderData = unserialize($orderData);
        if ($orderData) {

            app()->get('apiLogger')->log('Заказ найден');

            switch ($result) {
                case "extend":
                    $result = $this->prolongationOrder($tenantId, $orderId);
                    app()->get('apiLogger')->log('Продление заказа: {orderId}. {result}',
                        ['orderId' => $orderId, 'result' => $result ? 'Успешно' : 'Не успешно']);
                    break;
                case "cancel":
                    $result = $this->cancelOrder($tenantId, $orderId);
                    app()->get('apiLogger')->log('Отмена заказа: {orderId}. {result}',
                        ['orderId' => $orderId, 'result' => $result ? 'Успешно' : 'Не успешно']);
                    break;
                case "ok":
                    $this->okNotify($tenantId, $orderId);
                    app()->get('apiLogger')->log('Клиент дослушал уведомление');
                    break;
                case "noanwser":
                    $this->noAnwser($tenantId, $orderId);
                    app()->get('apiLogger')->log('Клиент не взял трубку');
                    break;
                case "nolisten":
                    $this->noListen($tenantId, $orderId);
                    app()->get('apiLogger')->log('Клиент не дослушал уведомление');
                    break;
                case "connectagent":
                    $this->connectAgent($tenantId, $orderId);
                    app()->get('apiLogger')->log('Клиент попросил соединить с оператором');
                    break;
                case "connectdriver":
                    $this->connectDriver($tenantId, $orderId);
                    app()->get('apiLogger')->log('Клиент попросил соединить с исполнителем');
                    break;
                default:
                    break;
            }
        } else {
            app()->get('apiLogger')->log('Заказ не найден');
        }
    }
    ///////////////////////////////Рабочие функции//////////////////////////////////////////////////

    /**
     * Продление заказа  (Заказ из просроченного id=52 возвращается в свободный id=4)
     *
     * @param $tenantId
     * @param $orderId
     *
     * @return bool
     */
    private function prolongationOrder_old($tenantId, $orderId)
    {
        //Обновляем инфу в мускуле
        $order = Order::findOne($orderId);
        //Обвновляем инфу в редисе
        $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);
        $orderData = unserialize($orderData);
        if (isset($orderData['status']['status_id'])) {
            if ($orderData['status']['status_id'] == 52) {
                $orderCityOffset = isset($orderData['time_offset']) ? $orderData['time_offset'] : 0;
                if ($order && $orderData) {
                    $order->status_id   = 4;
                    $order->status_time = time();
                    $order->order_time  = time() + $orderCityOffset + Order::getPickUp($tenantId, $order->city_id);
                    if ($order->save()) {
                        $orderData['status']['status_id']       = "4";
                        $orderData['status']['name']            = 'Free order';
                        $orderData['status']['status_group']    = 'new';
                        $orderData['status']['dispatcher_sees'] = "1";
                        $orderData['status_id']                 = "4";
                        $orderData['status_time']               = time();
                        $orderData['order_time']                = time() + $orderCityOffset + Order::getPickUp($tenantId,
                                $order->city_id);
                        $orderData                              = serialize($orderData);
                        $setResult                              = \Yii::$app->redis_orders_active->executeCommand('hset',
                            [$tenantId, $orderId, $orderData]);
                        if ($setResult == 1 || $setResult == 0) {
                            //Публикуем событие в редис
                            $publishResult = \Yii::$app->redis_pub_sub->executeCommand('PUBLISH',
                                ["order_" . $orderId, 'update_order_data']);
                            if ($publishResult < 1) {
                                Yii::error('Method prolongationOrder. BAD PABLISH RESULT:' . $publishResult);

                                return false;
                            }

                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    private function prolongationOrder($tenantId, $orderId)
    {
        //Достаем инфу в редисе
        $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);
        $orderData = unserialize($orderData);
        if (isset($orderData['status']['status_id'])) {
            if ($orderData['status']['status_id'] == 52) {
                $order = Order::findOne($orderId);

                if (!$order) {
                    return false;
                }

                $senderId = null;
                $lang     = 'ru';

                $requestId = \Yii::$app->request->headers->get('Request-Id');

                $orderUpdateEvent = new OrderUpdateEvent([
                    'requestId'      => $requestId,
                    'tenantId'       => $tenantId,
                    'orderId'        => $orderId,
                    'senderId'       => $senderId,
                    'lang'           => $lang,
                    'lastUpdateTime' => $order->update_time,
                    'sender'         => OrderUpdateEvent::SENDER_OPERATOR,
                ]);

                try {
                    $orderUpdateEvent->addEvent([
                        'status_id' => 4,
                    ]);

                    return true;
                } catch (QueueIsNotExistsException $ignore) {
                }
            }
        }

        return false;
    }

    /**
     * Добавляем мессагу в редис для диспетчера
     *
     * @param  $sipId
     * @param  $action
     * @param  $message
     */
    private function addMessageToDispatcher($sipId, $action, $message)
    {
        $activeDispatcherKeys = \Yii::$app->redis_active_dispetcher->executeCommand('KEYS', ['*']);
        if (is_array($activeDispatcherKeys) && !empty($activeDispatcherKeys)) {
            $activeDispatcherVals = \Yii::$app->redis_active_dispetcher->executeCommand('MGET', $activeDispatcherKeys);
            if (!empty($activeDispatcherVals)) {
                foreach ($activeDispatcherVals as $dispetcherDataSerr) {
                    $dispetcherData = unserialize($dispetcherDataSerr);
                    if (is_array($dispetcherData)) {
                        if ($dispetcherData["sip_id"] == $sipId) {
                            $dispatcherId = isset($dispetcherData['user_id']) ? $dispetcherData['user_id'] : null;
                            if ($dispatcherId) {
                                $data = [
                                    'action'  => $action,
                                    'message' => $message,
                                ];
                                $data = serialize($data);
                                \Yii::$app->redis_dispetcher_live_notice->executeCommand('SET', [$dispatcherId, $data]);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *  Постановка в очередь или мгновенное распределение вызова
     *  Порядок действий:
     *   1) Проверить в redis список свободных операторов для данного номера по did,
     *
     *      если есть забрать от туда оператора и вернуть в ATC response  connectAgent с
     *
     *      sipid оператора из redis.
     *
     *   2) Если свободные операторы отсутствуют - звонок записать в конец списка очереди звонков в redis.
     *
     *   3) Освободившиеся операторы вызывая специальный метод - устанавливают свой статус в ожидание,
     *
     *      после чего забирают звонки из очереди, если звонков нет - встают в очередь операторов.
     *   4) Если освобождается оператор готовый забрать какой-то из вызовов - выполняется вызов
     *
     *       метода АТС - соединить с оператором(см ниже).
     *
     * @param array $scenario - описание сценария
     * @param array $callData
     * $return array
     *
     * array(
     *
     *      'scenario' => array(
     *
     *          'action'  => 'doQueue',
     *
     *          'type'    =>  'r',
     *
     *          'timeout' => '10',
     *
     *      ),
     *
     *      'queueId' => ''
     *
     * )
     * @return array
     */
    private function queueAction($scenario, $callData)
    {

        //В редис идем в db6 (очередь операторов) далее:
        //1)LLEN  - сколько свободных операторов
        //2) если несколько то в цикле
        // по value ищем в db5 диспетчера, ищем city_id, если есть то:
        // делаем LSET 'list' index value (value = null) в db6
        // удаляем из редиса LREM 'list' 0 null  в db6
        //\Yii::$app->redis_queue_call->executeCommand('rpush', ['68_26068', 'media1_213124124123000']);
        //\Yii::$app->redis_queue_call->executeCommand('lpop', ['68_26068']);
        $dispetcherQueueId  = $callData['tenantId'];
        $callCityId         = $callData['cityId'];
        $dispetcherQueueLen = \Yii::$app->redis_free_dispetcher_queue->executeCommand('LLEN', [$dispetcherQueueId]);
        //Если нет свободных операторов добавляем звонок в очередь звонков

        app()->get('apiLogger')->log('Ищу свободных операторов. Найдено {count}', ['count' => $dispetcherQueueLen]);

        if ($dispetcherQueueLen == 0) {
            $callQueueId = $callData['tenantId'] . '_' . $callData['cityId'];
            $callId      = $callData['id'];
            Yii::$app->redis_queue_call->executeCommand('rpush', [$callQueueId, $callId]);
            $response = [
                'scenario' => $scenario,
                'queueId'  => $callQueueId,
            ];

            app()->get('apiLogger')->log('Ставлю звонок {callId} в очередь', ['callId' => $callId]);

            return $response;
        }
        //Если есть свободные операторы то перебираем их,
        for ($i = 0; $i < $dispetcherQueueLen; $i++) {
            $dispetcherId   = \Yii::$app->redis_free_dispetcher_queue->executeCommand('LINDEX',
                [$dispetcherQueueId, $i]);
            $dispetcherData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$dispetcherId]);
            $dispetcherData = unserialize($dispetcherData);
            $cityArr        = (array)$dispetcherData['city_arr'];
            foreach ($cityArr as $city => $cityId) {
                if ($callCityId == $cityId) {
                    //Удаляем из дичпетчера из очереди
                    \Yii::$app->redis_free_dispetcher_queue->executeCommand('LSET', [$dispetcherQueueId, $i, 'null']);
                    \Yii::$app->redis_free_dispetcher_queue->executeCommand('LREM', [$dispetcherQueueId, 0, 'null']);
                    // Отвечаем соединить с оператором
                    $scenario = [
                        'action'  => 'connectAgent',
                        'agent'   => $dispetcherData['sip_id'],
                        'type'    => isset($scenario['type']) ? $scenario['type'] : 'r',
                        'timeout' => $scenario['timeout'] ? $scenario['timeout'] : '10',
                    ];

                    $response = [
                        'scenario' => $scenario,
                        'queueId'  => null,
                    ];

                    app()->get('apiLogger')->log('Соединяю с оператором {operator}',
                        ['operator' => $dispetcherData['sip_id']]);

                    return $response;
                }
            }
        }

        // Если не нашли свободных диспетчеров в этом городе возвращаем сценарий постановки  в очередь
        $callQueueId = $callData['tenantId'] . '_' . $callData['cityId'];
        $callId      = $callData['id'];
        Yii::$app->redis_queue_call->executeCommand('rpush', [$callQueueId, $callId]);

        $response = [
            'scenario' => $scenario,
            'queueId'  => $callQueueId,
        ];

        app()->get('apiLogger')->log('Ставлю звонок {callId} в очередь', ['callId' => $callId]);

        return $response;
    }

    /**
     * Получение сценария вызова для автотвечика
     * По номеру звонка ищет заказ, если есть и один то отдаем сценарий отзвона, иначе null
     *
     * @param  $tenantId
     * @param  $phone
     *
     * @return array
     */
    private function getInfoScenarioByPhone($tenantId, $phone)
    {
        $clientOrder = Client::getClientOrders($tenantId, $phone);
        if (count($clientOrder) == "1") {
            $order    = isset($clientOrder[0]) ? $clientOrder[0] : null;
            $orderId  = isset($order["order_id"]) ? $order["order_id"] : null;
            $statusId = isset($order["status"]["status_id"]) ? $order["status"]["status_id"] : null;
            if (!empty($orderId) && !empty($statusId)) {
                if ($order['status']['status_group'] == 'pre_order') {
                    app()->apiLogger->log('Предварительный заказ {orderId}', ['orderId' => $orderId]);

                    return [];
                }
                $notifyData = $this->getNotifyData($tenantId, $orderId, $phone, $statusId);
                if (!empty($notifyData)) {
                    $notifyData['action'] = "doInfo";

                    return $notifyData;
                }
            }
        }
    }

    /**
     * Получиние массива для уведомления клиента
     *
     * @param string $tenantId
     * @param string $orderId
     * @param string $phone
     * @param string $statusId
     *
     * @return array
     */
    private function getNotifyData($tenantId, $orderId, $phone, $statusId)
    {
        $domain    = Tenant::getDomain($tenantId);
        $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);
        $orderData = unserialize($orderData);
        if (empty($orderData)) {
            $orderData = Order::findOrderFromMysql($orderId);
        }
        if (isset($tenantId) && isset($orderId) && isset($phone) && isset($orderData) && isset($domain)) {
            $phoneLine       = PhoneLine::getPhoneLine($tenantId, $orderData["city_id"], $orderData["tariff_id"]);
            if (!$phoneLine) {
                return null;
            }
            $callCountTry    = TenantSetting::getTenantSettingValueByName($tenantId, "CALL_COUNT_TRY",
                $orderData["city_id"]);
            $callWaitTimeOut = TenantSetting::getTenantSettingValueByName($tenantId, "CALL_WAIT_TIMEOUT",
                $orderData["city_id"]);
            $autoCall        = new AutocallFile(["tenant_id" => $tenantId, "order_id" => $orderId]);

            $files = $autoCall->getFiles();
            if ($files === null) {
                return null;
            }
            $filterdFiles = $this->formatAudioFiles($files);
            $notifyData   = [
                'order_id'          => (string)$orderId,
                'status_id'         => (string)$statusId,
                'domain'            => (string)$domain,
                'did'               => (string)$phone,
                'welcome'           => (string)$filterdFiles['welcome'],
                'orderfiles'        => (string)$filterdFiles['orderfiles'],
                'repeatfiles'       => (string)$filterdFiles['repeatfiles'],
                'driverfile'        => (string)$filterdFiles['driverfile'],
                'agentfile'         => (string)$filterdFiles['agentfile'],
                'wrongkey'          => (string)$filterdFiles['wrongkey'],
                'successfile'       => (string)$filterdFiles['successfile'],
                'cancel'            => (string)$filterdFiles['cancel'],
                'driver_phone'      => isset($orderData["worker"]["phone"]) ? (string)$orderData["worker"]["phone"] : null,
                'phone_line'        => (string)$phoneLine,
                'call_count_try'    => isset($callCountTry) ? (string)$callCountTry : (string)1,
                'call_wait_timeout' => isset($callWaitTimeOut) ? (string)$callWaitTimeOut : (string)20,
            ];

            if ($statusId == 17) {
                $notifyData['company_phone'] = isset($orderData["worker"]["tenant_company"]["phone"]) ? (string)$orderData["worker"]["tenant_company"]["phone"] : null;
                $notifyData['order_number']  = (string)ArrayHelper::getValue($orderData, 'order_number');
            }

            return $notifyData;
        }

        return null;
    }

    //Получение рандомного сервера для  нотификации
    private function getNotifySereverUrl()
    {
        $serversArr   = Yii::$app->params['notifyServers'];
        $randomServer = $serversArr[array_rand($serversArr)];

        return $randomServer;
    }

    /**
     * Уведомить клиента
     *
     * @param $tenantId
     * @param $orderId
     */
    private function logCallNotify($tenantId, $orderId)
    {
        $orderChange                     = new OrderChangeData();
        $orderChange->order_id           = $orderId;
        $orderChange->tenant_id          = $tenantId;
        $orderChange->change_field       = "status_id";
        $orderChange->change_object_id   = $orderId;
        $orderChange->change_object_type = "order";
        $orderChange->change_val         = "100";
        $orderChange->change_subject     = "system";
        $orderChange->change_time        = (int)time();
        $orderChange->save();
    }

    /**
     * Форматирование аудио файлов
     *
     * @param $fileArray
     *
     * @return array
     */
    private function formatAudioFiles($fileArray)
    {
        $filterdFiles = [
            'welcome'     => [],
            'orderfiles'  => [],
            'repeatfiles' => [],
            'driverfile'  => [],
            'agentfile'   => [],
            'wrongkey'    => [],
        ];
        if (is_array($fileArray)) {
            foreach ($fileArray as $fileGroupKey => $fileGroupValue) {
                $fileString = "";
                if (is_array($fileGroupValue)) {
                    foreach ($fileGroupValue as $file) {
                        if (is_array($file)) {
                            $fileName   = str_replace(".wav", "", $file["name"]);
                            $group      = $file["group"];
                            $fileString = $fileString . 'ivr/' . $group . '/' . $fileName . ',';
                        }
                    }
                }
                if (!empty($fileString)) {
                    $fileString = substr($fileString, 0, -1);
                }
                $filterdFiles[$fileGroupKey] = $fileString;
            }
        }

        return $filterdFiles;
    }


    /**
     * Соединить с водителем
     *
     * @param $tenantId
     * @param $orderId
     */
    private function connectDriver($tenantId, $orderId)
    {
        $orderChange                     = new OrderChangeData();
        $orderChange->order_id           = $orderId;
        $orderChange->tenant_id          = $tenantId;
        $orderChange->change_field       = "status_id";
        $orderChange->change_object_id   = $orderId;
        $orderChange->change_object_type = "order";
        $orderChange->change_val         = "104";
        $orderChange->change_subject     = "system";
        $orderChange->change_time        = (int)time();
        $orderChange->save();
    }

    /**
     * Соединить с диспетчером
     *
     * @param $tenantId
     * @param $orderId
     */
    private function connectAgent($tenantId, $orderId)
    {
        $orderChange                     = new OrderChangeData();
        $orderChange->order_id           = $orderId;
        $orderChange->tenant_id          = $tenantId;
        $orderChange->change_field       = "status_id";
        $orderChange->change_object_id   = $orderId;
        $orderChange->change_object_type = "order";
        $orderChange->change_val         = "105";
        $orderChange->change_subject     = "system";
        $orderChange->change_time        = (int)time();
        $orderChange->save();
    }


    /**
     * Клиент не дослушал
     *
     * @param $tenantId
     * @param $orderId
     */
    private function noListen($tenantId, $orderId)
    {
        $callWarningId                   = "102";
        $orderChange                     = new OrderChangeData();
        $orderChange->order_id           = $orderId;
        $orderChange->tenant_id          = $tenantId;
        $orderChange->change_field       = "status_id";
        $orderChange->change_object_id   = $orderId;
        $orderChange->change_object_type = "order";
        $orderChange->change_val         = $callWarningId;
        $orderChange->change_subject     = "system";
        $orderChange->change_time        = (int)time();
        $orderChange->save();
        $this->setCallWarning($tenantId, $orderId, $callWarningId);
    }


    /**
     * Не берет трубку
     *
     * @param $tenantId
     * @param $orderId
     */
    private function noAnwser($tenantId, $orderId)
    {
        $callWarningId                   = "103";
        $orderChange                     = new OrderChangeData();
        $orderChange->order_id           = $orderId;
        $orderChange->tenant_id          = $tenantId;
        $orderChange->change_field       = "status_id";
        $orderChange->change_object_id   = $orderId;
        $orderChange->change_object_type = "order";
        $orderChange->change_val         = $callWarningId;
        $orderChange->change_subject     = "system";
        $orderChange->change_time        = (int)time();
        $orderChange->save();
        $this->setCallWarning($tenantId, $orderId, $callWarningId);
    }

    /**
     * Отменил заказ
     *
     * @param $tenantId
     * @param $orderId
     * @param $orderData
     */
    private function cancelOrder_old($tenantId, $orderId, $orderData)
    {
        $orderData['status_id']                 = "39";
        $orderData['status']['status_id']       = "39";
        $orderData['status']['name']            = "Clients refusing";
        $orderData['status']['status_group']    = "rejected";
        $orderData['status']['dispatcher_sees'] = "1";
        $orderData['status_time']               = time();
        $orderDataNew                           = serialize($orderData);
        $hsetResult                             = \Yii::$app->redis_orders_active->executeCommand('hset',
            [$tenantId, $orderId, $orderDataNew]);
        if ($hsetResult == 1 || $hsetResult == 0) {
            $order = Order::findOne($orderId);
            if ($order) {
                $order->status_id   = 39;
                $order->status_time = time();
                if ($order->save()) {
                    //Публикуем событие в редис
                    \Yii::$app->redis_pub_sub->executeCommand('PUBLISH', ["order_" . $orderId, 'update_order_data']);
                } else {
                    Yii::error($order->errors);
                }
            }
        }
    }

    private function cancelOrder($tenantId, $orderId, $orderData)
    {
        $order = Order::findOne($orderId);

        if (!$order) {
            return false;
        }

        $senderId = $order->client_id;
        $lang     = 'ru';

        $requestId = \Yii::$app->request->headers->get('Request-Id');

        $orderUpdateEvent = new OrderUpdateEvent([
            'requestId'      => $requestId,
            'tenantId'       => $tenantId,
            'orderId'        => $orderId,
            'senderId'       => $senderId,
            'lang'           => $lang,
            'lastUpdateTime' => $order->update_time,
            'sender'         => OrderUpdateEvent::SENDER_CLIENT,
        ]);

        try {
            $orderUpdateEvent->addEvent([
                'status_id' => 39,
            ]);

            return true;
        } catch (QueueIsNotExistsException $ignore) {
        }

        return false;
    }


    /**
     * Прослушал
     *
     * @param $tenantId
     * @param $orderId
     */
    private function okNotify($tenantId, $orderId)
    {
        $orderChange                     = new OrderChangeData();
        $orderChange->order_id           = $orderId;
        $orderChange->tenant_id          = $tenantId;
        $orderChange->change_field       = "status_id";
        $orderChange->change_object_id   = $orderId;
        $orderChange->change_object_type = "order";
        $orderChange->change_val         = "101";
        $orderChange->change_subject     = "system";
        $orderChange->change_time        = (int)time();
        $orderChange->save();
        $this->unsetCallWarning($tenantId, $orderId);
    }

    /**
     *
     * @param  $tenantId
     * @param  $orderId
     * @param  $warningId
     */
    private function setCallWarning_old($tenantId, $orderId, $warningId)
    {
        $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);
        $orderData = unserialize($orderData);
        if ($orderData) {
            $orderData["call_warning_id"] = $warningId;
            $orderData                    = serialize($orderData);
            $hsetResult                   = \Yii::$app->redis_orders_active->executeCommand('hset',
                [$tenantId, $orderId, $orderData]);
            if ($hsetResult == 0 || $hsetResult == 1) {
                $order                  = Order::findOne($orderId);
                $order->call_warning_id = $warningId;
                $order->save();
            }
        }
    }

    private function setCallWarning($tenantId, $orderId, $warningId)
    {
        $order = Order::findOne($orderId);

        if (!$order) {
            return;
        }

        $senderId = null;
        $lang     = 'ru';

        $requestId = \Yii::$app->request->headers->get('Request-Id');

        $orderUpdateEvent = new OrderUpdateEvent([
            'requestId'      => $requestId,
            'tenantId'       => $tenantId,
            'orderId'        => $orderId,
            'senderId'       => $senderId,
            'lang'           => $lang,
            'lastUpdateTime' => $order->update_time,
            'sender'         => OrderUpdateEvent::SENDER_OPERATOR,
        ]);

        try {
            $orderUpdateEvent->addEvent([
                'call_warning_id' => $warningId,
            ]);
        } catch (QueueIsNotExistsException $ignore) {
        }
    }

    /**
     *
     * @param  $tenantId
     * @param  $orderId
     */
    private function unsetCallWarning_old($tenantId, $orderId)
    {
        $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);
        $orderData = unserialize($orderData);
        if ($orderData) {
            $orderData["call_warning_id"] = null;
            $orderData                    = serialize($orderData);
            $hsetResult                   = \Yii::$app->redis_orders_active->executeCommand('hset',
                [$tenantId, $orderId, $orderData]);
            if ($hsetResult == 0 || $hsetResult == 1) {
                $order                  = Order::findOne($orderId);
                $order->call_warning_id = null;
                $order->save();
            }
        }
    }

    private function unsetCallWarning($tenantId, $orderId)
    {
        $order = Order::findOne($orderId);

        if (!$order) {
            return;
        }

        $senderId = null;
        $lang     = 'ru';

        $requestId = \Yii::$app->request->headers->get('Request-Id');

        $orderUpdateEvent = new OrderUpdateEvent([
            'requestId'      => $requestId,
            'tenantId'       => $tenantId,
            'orderId'        => $orderId,
            'senderId'       => $senderId,
            'lang'           => $lang,
            'lastUpdateTime' => $order->update_time,
            'sender'         => OrderUpdateEvent::SENDER_OPERATOR,
        ]);

        try {
            $orderUpdateEvent->addEvent([
                'call_warning_id' => null,
            ]);
        } catch (QueueIsNotExistsException $ignore) {
        }
    }

    /**
     * Отправка звонка диспечтеру
     *
     * @param  $pbxHost
     * @param  $callId
     * @param  $agent
     * @param  $timeout
     * @param  $type
     *
     * @return array|boolean    ['status' => 'restidnotfound',]
     */
    private function sendCallToDispetcher($pbxHost, $callId, $agent, $timeout, $type)
    {
        app()->get('apiLogger')->log('Назначаю оператору {operator} звонок {callId}',
            ['operator' => $agent, 'callId' => $callId]);

        $params = [
            'callid'  => $callId,
            'agent'   => $agent,
            'timeout' => $timeout,
            'type'    => $type,
        ];

        $protocol = Yii::$app->params['protokol'];
        $pbxPort  = Yii::$app->params['pbxPort'];

        app()->get('apiLogger')->log('Запрос на сервер {url}',
            ['url' => "{$protocol}://{$pbxHost}:{$pbxPort}/call/connectagent"]);
        app()->get('apiLogger')->log('Request: {data}', ['data' => json_encode($params, JSON_UNESCAPED_SLASHES)]);

        try {
            $client = new \GuzzleHttp\Client();

            $url       = "{$protocol}://{$pbxHost}:{$pbxPort}/call/connectagent";
            $options   = [
                'query'   => $params,
                'headers' => [
                    'Request-Id' => app()->get('apiLogger')->getId(),
                ],
                'timeout' => 5,
            ];
            $startTime = microtime(true);
            $response  = $client->request('GET', $url, $options);
            $timeout   = microtime(true) - $startTime;
            $result    = json_decode($response->getBody(), true);

            app()->get('apiLogger')->log('Запрос на сервер занял {ms} секунд', ['ms' => round($timeout, 4)]);
            app()->get('apiLogger')->log('Response: {data}', ['data' => json_encode($result, JSON_UNESCAPED_SLASHES)]);

            return $result;
        } catch (\Exception $ex) {
            app()->get('apiLogger')->log('Неудачный запрос. {ex}', ['ex' => get_class($ex)]);
            Yii::error($ex);

            return [];
        }
    }

    /**
     * Удалить звонок из редиса
     *
     * @param array $callData
     */
    private function deleteCall($callData)
    {
        // Ид звонка
        $callId = $callData['id'];
        //Удаляем из списка активных звонков
        \Yii::$app->redis_active_call->executeCommand('DEL', [$callId]);

        app()->get('apiLogger')->log('Удаляю звонок {callId} из БД', ['callId' => $callId]);

        // Если есть звонок в очереди удаляем его
        if (!empty($callData['queue'])) {
            // Ид очереди
            $callQueueId = $callData['queue'];

            // Длина очереди звонков
            $callQueueLen = \Yii::$app->redis_queue_call->executeCommand('LLEN', [$callQueueId]);
            if ($callQueueLen != 0) {
                // Обходим очередь звонков
                for ($i = 0; $i < $callQueueLen; $i++) {
                    // получаем ид звонка из очереди
                    $id = \Yii::$app->redis_queue_call->executeCommand('LINDEX', [$callQueueId, $i]);
                    if ($callId == $id) {
                        //Удаляем звонок из очереди
                        \Yii::$app->redis_queue_call->executeCommand('LSET', [$callQueueId, $i, 'null']);
                        \Yii::$app->redis_queue_call->executeCommand('LREM', [$callQueueId, 0, 'null']);

                        app()->get('apiLogger')->log('Удаляю звонок {callId} из очереди', ['callId' => $callId]);
                    }
                }
            }
        }
    }

    /**
     * @param $array
     *
     * @return array
     */
    private function getParams($array)
    {
        $params = $array;
        unset($params['q']);
        $requset = new TaxiApiRequest();

        return $requset->filterParams($params);
    }


    private function externalNotifyClient(array $params)
    {
        if (ArrayHelper::getValue($params, 'call_wait_timeout', 0) <= 0
            || ArrayHelper::getValue($params, 'call_count_try', 0) <= 0) {
            return [
                'result' => 0,
            ];
        }

        $client = new \GuzzleHttp\Client([
            'timeout'         => 5,
            'connect_timeout' => 5,
            'headers'         => [
                'Content-Type'  => 'application/x-www-form-urlencoded',
                'Authorization' => 'Basic d2ViZ29vdGF4OlNhWFlvaXQ3',
                'Cache-Control' => 'no-cache',
            ],
        ]);

        $options = [
            'body' => 'scriptname=GootaxCallIVR&startparam1={"callparams": [' . json_encode($params) . ']}',
        ];

        try {
            $response = $client->request('POST', 'http://sipshara4.telsystems.com.ua/execsvcscriptplain', $options);

            return [
                'result' => (string)$response->getBody() === 'ok' ? 1 : 0,
            ];

        } catch (GuzzleException $exception) {
            app()->get('apiLogger')->error('Error: {message}', ['message' => $exception->getMessage()]);

            return [
                'result' => 0,
            ];
        }
    }
}