<?php
namespace api\controllers;

use app\components\taxiApiRequest\TaxiApiRequest;
use app\models\order\Order;
use app\models\tenant\Tenant;
use app\models\worker\TenantCompany;
use app\models\worker\TenantHasSms;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class PhoneController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'notify_company'         => ['get'],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();
    }

    public function actionNotify_company(){

        $params   = $this->getParams($_GET);
        $orderId  = isset($params['order_id']) ? $params['order_id'] : null;
        $tenantId = isset($params['tenant_id']) ? (int)$params['tenant_id'] : null;
        $company_id    = isset($params['company_id']) ? $params['company_id'] : null;
        $response = [
            'result' => 1,
        ];

        return json_encode($response);

        /*$company = $this->getCompany($company_id);
        $tenant = $this->getTenant();
        if(!empty($company['phone']) && ($tenant['contact_phone'])){
           // $company['phone'] = '79772602817';
            ///$company['phone'] = '79164099255';

            $dateprovider = $this->getApiMessageBird();
            list($voise, $text) =  $this->getPrase($orderId);

            $MessageBird = new \MessageBird\Client($dateprovider['password']);

            // todo VOICE
            $VoiceMessage             = new \MessageBird\Objects\VoiceMessage();
            $VoiceMessage->recipients =  [
                $company['phone']
            ];
            $VoiceMessage->originator = $tenant['contact_phone'];
            $VoiceMessage->body = $voise;
            $VoiceMessage->language = 'de-de';
            $VoiceMessage->voice = 'female';
            $VoiceMessage->ifMachine = 'continue';


            // todo SMS
            $Message             = new \MessageBird\Objects\Message();
            $Message->originator = $tenant['company_name'];
            $Message->recipients = [
                $company['phone']
            ];
            $Message->body       = $text;
            $Message->datacoding = 'unicode';
            try {
                $VoiceMessageResult = $MessageBird->voicemessages->create($VoiceMessage);
                $MessageResult = $MessageBird->messages->create($Message);

                $response = [
                    'result' => 1,
                ];
            } catch (\Exception $e) {
                Yii::error($e);
                $response = [
                    'result' => 0,
                ];
            }

        }
        return json_encode($response);*/
    }

    public function getPrase($order_id){

        $order = Order::find()
            ->where([
                'order_id'     => $order_id,
            ])
            ->one();

        $number_order = $order->order_number;

        $str_vs = ' Sie haben die Bestellung № ' .$number_order . ' Zuweisen Sie bitte dem Fahrer!' ;
        $str_vs .= ' Man kann dem Fahrer per Seite zuweisen!' ;

        $str_sms = ' Sie haben die Bestellung № ' .$number_order . '. Zuweisen Sie bitte dem Fahrer. ' ;
        $str_sms .= 'Man kann dem Fahrer per Seite zuweisen.' ;

        return [$str_vs,$str_sms ];

    }

    public function getApiMessageBird(){
        $message_dird = TenantHasSms::find()
            ->where([
                'default' => 1
            ])
            ->asArray()
            ->one();

        return $message_dird;
    }

    public function getCompany($company_id){
        $company = TenantCompany::find()
            ->where([
                'tenant_company_id' => $company_id
            ])
            ->asArray()
            ->one();

        return $company;
    }

    public function getTenant(){
        $tenant = Tenant::find()
            ->asArray()
            ->one();
        return isset($tenant) ? $tenant : null;
    }


    public function actionTest(){
        $byte_array = unpack('C*', 'Daa sd');
        var_dump($byte_array);
       // var_dump(openssl_get_cipher_methods());
        exit();

    }


    /**
     * @param $array
     *
     * @return array
     */
    private function getParams($array)
    {
        $params = $array;
        unset($params['q']);
        $requset = new TaxiApiRequest();

        return $requset->filterParams($params);
    }

}

/*
abstract class Component
{
    abstract public function Operation();
}
class ConcreteComponent extends Component
{
    public function Operation()
    {
        return 'I am component';
    }
}
abstract class Decorator extends Component
{
    protected
        $_component = null;
    public function __construct(Component $component)
    {
        $this -> _component = $component;
    }
    protected function getComponent()
    {
        return $this -> _component;
    }
    public function Operation()
    {
        return $this -> getComponent() -> Operation();
    }
}
class ConcreteDecoratorA extends Decorator
{
    public function Operation()
    {
        return '<a>' . parent::Operation() . '</a>';
    }
}
class ConcreteDecoratorB extends Decorator
{
    public function Operation()
    {
        return '<strong>' . parent::Operation() . '</strong>';
    }
}

$Element = new ConcreteComponent();
$ExtendedElement = new ConcreteDecoratorA($Element);
$SuperExtendedElement = new ConcreteDecoratorB($ExtendedElement);
print $SuperExtendedElement -> Operation();*/


