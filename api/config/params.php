<?php

return [
    'adminEmail'    => 'api_phone_call@gootax.pro',
    'pbxPort'       => '8088',
    'pbxHost'       => [
        'media1' => '192.168.81.9',
    ],
    'protokol'      => 'http',
    'notifyServers' => ['http://192.168.81.9:8088/call/notify'],
    'version' => require __DIR__ . '/version.php',
];
