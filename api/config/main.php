<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/params.php')
);

return [
    'id'                  => 'api',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'api\controllers',
    'components'          => [
//        'apiLogger'                    => [
//            'class' => logger\ApiLogger::class,
//        ],
        'loggerTarget' => [
            'class'    => \api\components\apiLogger\target\SyslogTarget::className(),
            //            'class'    => \api\components\apiLogger\target\FakeTarget::className(),
            'identity' => 'api_phone_call-' . $params['version'],
         ],
        'apiLogger' => [
            'class'  => \api\components\apiLogger\Logger::className(),
            'target' => 'loggerTarget',
         ],
        'amqp'                        => [
            'class'    => 'app\components\rabbitmq\Amqp',
            'host'     => '192.168.81.22',
            'port'     => 5672,
            'user'     => 'guest',
            'password' => 'guest',
        ],
        'i18n'                         => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
            ],
        ],
        'user'                         => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'mailer'                       => [
            'class'            => 'yii\swiftmailer\Mailer',
            // it doesn't matter if it will write to file or actually send email, right?
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => 'smtp.yandex.ru',
                'port'       => 465,
                'username'   => 'api_phone_call@gootax.pro',
                'password'   => '',
                'encryption' => 'ssl',
            ],
        ],
        'log'                          => [
            'traceLevel' => 3,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer',
                    'levels'  => ['error', 'warning'],
                    'message' => [
                        'from'    => ['api_phone_call@gootax.pro'],
                        'to'      => ['api_phone_call@gootax.pro'],
                        'subject' => 'Error in KABBI',
                    ],
                ],
            ],
        ],
        'errorHandler'                 => [
            'errorAction' => 'call/error',
        ],
        'urlManager'                   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            //'suffix' => '.html',
            'rules'           => [
                ''         => 'api/index',
                '<action>' => 'api/<action>',
            ],
        ],
        'assetManager'                 => [
            'basePath' => '@webroot/assets',
            'baseUrl'  => '@web/assets',
        ],
        'request'                      => [
            'baseUrl'             => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ymB2hl6GmkDJkoB_TnQrz_RzzsfoCISp',
        ],
        'redis_drivers'                => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 0,
        ],
        'redis_orders_active'          => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 1,
        ],
        'redis_phone_scenario'         => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 2,
        ],
        'redis_active_call'            => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 3,
        ],
        'redis_queue_call'             => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 4,
        ],
        'redis_active_dispetcher'      => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 5,
        ],
        'redis_free_dispetcher_queue'  => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 6,
        ],
        'redis_client_active_orders'   => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 9,
        ],
        'redis_dispetcher_live_notice' => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
            'database' => 11,
        ],
        'redis_pub_sub'                => [
            'class'    => 'yii\redis\Connection',
            'hostname' => '192.168.81.13',
            'port'     => 6379,
        ],
    ],
    'params'              => $params,
];
