<?php

namespace app\components\taxiApiRequest;

use yii\base\Object;
use Yii;
use DateTime;
use app\components\taxiApiRequest\TaxiApiMethods;
use app\components\taxiApiRequest\TaxiErrorCode;
use yii\db\ActiveRecord;

/**
 */
class TaxiApiRequest extends Object
{

    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function filterParams($params)
    {
        $paramsFiltered = array();
        foreach ($params as $paramKey => $paramVal) {
            if (is_string($paramVal)) {
                $paramVal = urldecode($paramVal);
                $paramVal = mb_strcut($paramVal, 0, 512);
                $paramVal = str_replace('\\', '/', $paramVal);
                $paramVal = trim(strip_tags(stripcslashes($paramVal)));
            }
            $paramsFiltered[$paramKey] = $paramVal;
        }
        return $paramsFiltered;
    }

    public function getJsonAnswer($result)
    {
        return json_encode($result);
    }

}
