<?php

namespace api\components\apiLogger\target;

interface LogTargetInterface
{
    public function export($message, $severity);
}