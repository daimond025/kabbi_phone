<?php

namespace api\components\apiLogger\target;

use yii\base\Component;

class SyslogTarget extends Component implements LogTargetInterface
{
    /**
     * @var string
     */
    public $identity;

    public function export($message, $severity)
    {
        openlog($this->identity, LOG_ODELAY | LOG_PID, LOG_USER);
        syslog($severity, $message);
        closelog();
    }
}