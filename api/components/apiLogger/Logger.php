<?php

namespace api\components\apiLogger;

use api\components\apiLogger\target\LogTargetInterface;
use Ramsey\Uuid\Uuid;
use yii\base\Component;

/**
 * Class Logger
 * @package api\modules\v1\components\logger
 *
 * @property string             $id
 * @property LogTargetInterface $target
 * @property string             $identity
 */
class Logger extends Component
{
    public $id = null;
    public $callId = null;
    public $target;
    public $identity;

    private static $startTime;
    private $isSendHeader;

    const TYPE_MAIN = 'main';
    const TYPE_HEAD = 'request';

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->id           = Uuid::uuid4()->toString();
        $this->target       = app()->get($this->target);
        $this->isSendHeader = false;
        self::$startTime    = self::getSystemTimes();
    }

    public function setId($value)
    {
        $this->id = $value;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCallId($value)
    {
        $this->callId = $value;
    }

    public function getCallId()
    {
        return $this->callId;
    }

    public function requestInfo()
    {
        $this->isSendHeader = true;

        $method     = app()->request->getMethod();
        $path       = app()->request->getPathInfo();
        $statusCode = app()->response->statusCode;
        $statusText = app()->response->statusText;

        $params = app()->request->isPost ? post() : get();

        $paramsAsString = [];
        foreach ($params as $param => $value) {
            if (is_string($value)) {
                $paramsAsString[] = "{$param}=\"{$value}\"";
            }
        }
        $info = "{$_SERVER['REMOTE_ADDR']}:{$_SERVER['REMOTE_PORT']} -> \"{$method} /{$path} {$_SERVER['SERVER_PROTOCOL']}\" {$statusCode} {$statusText} "
            . "(" . implode(",", $paramsAsString) . ") " . $this->getStatistic();

        $this->log($info, [], self::TYPE_HEAD);
    }

    public function error($message, $params = [], $class = self::TYPE_MAIN)
    {
        $this->log($message, $params, $class, LOG_ERR);
    }

    public function log($message, $params = [], $class = self::TYPE_MAIN, $severity = LOG_INFO)
    {
        if (!$this->isSendHeader) {
            $this->requestInfo();
        }

        $message = ($message === null) ? $message : $this->interpolate($message, $params);

        $this->export($class, $message, $severity);
    }

    protected function export($class, $message, $severity = LOG_INFO)
    {
        $this->target->export($this->getContent($class, $message), $severity);
    }

    protected function getContent($class, $message)
    {
        $params['request-id'] = $this->id;
        if(!empty($this->callId)) {
            $params['call-id'] = $this->callId;
        }

        array_walk($params, function(&$item, $key) {
            $item = $key . '=' . $item;
        });

        $paramsString = implode(',', $params);
        return "[$class] {$paramsString} {$message}";
    }

    /**
     * Interpolate log message
     *
     * @param string $message
     * @param array  $params
     *
     * @return string
     */
    protected function interpolate($message, array $params = [])
    {
        $replace = [];
        foreach ($params as $key => $val) {
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        return strtr($message, $replace);
    }

    private function getStatistic()
    {
        $size = $this->getServerParam('CONTENT_LENGTH', 0);

        list($time, $utime, $stime) = array_map(function ($a, $b) {
            return ($b - $a) / 1000.0;
        }, self::$startTime, self::getSystemTimes());

        $memory = round(memory_get_peak_usage() / 1024, 3);

        return sprintf('n:%db t:%.3fms u:%.3fms s:%.3fms m:%dkb', $size, $time, $utime, $stime, $memory);
    }

    private function getServerParam($param, $defaultValue = '')
    {
        return array_key_exists($param, $_SERVER) ? $_SERVER[$param] : $defaultValue;
    }

    private static function getSystemTimes()
    {
        $data = getrusage();

        return [
            microtime(true) * 1e6,
            $data['ru_utime.tv_sec'] * 1e6 + $data['ru_utime.tv_usec'],
            $data['ru_stime.tv_sec'] * 1e6 + $data['ru_stime.tv_usec'],
        ];
    }
}