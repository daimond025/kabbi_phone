<?php

namespace api\modules\v1\requests;

/**
 * Class SetStateAsteriskRequest
 * @package api\modules\v1\actions
 *
 * @var string $agent
 * @var string $state
 * @var string $reason
 * @var string $requestId
 */
class SetStateAsteriskRequest extends BaseRequest
{
    const STATE_UNAVAILABLE = 'unavailable';
    const STATE_AVAILABLE = 'available';
    const STATE_BUSY = 'busy';
    const STATE_FREE = 'free';

    public function attributes()
    {
        return [
            'agent'     => [
                'type' => self::TYPE_GET,
                'name' => 'agent',
            ],
            'state'     => [
                'type' => self::TYPE_GET,
                'name' => 'state',
            ],
            'reason'    => [
                'type'    => self::TYPE_GET,
                'name'    => 'reason',
                'default' => '',
            ],
            'requestId' => [
                'type' => self::TYPE_HEADER,
                'name' => 'request-id',
            ],
        ];
    }

    public function rules()
    {
        return [
            [['agent', 'state'], 'required'],
            ['state', 'in', 'range' => self::getStateList()],
            ['reason', 'string', 'max' => 255],
            ['requestId', 'safe'],
        ];
    }

    public static function getStateList()
    {
        return [
            self::STATE_UNAVAILABLE,
            self::STATE_AVAILABLE,
            self::STATE_BUSY,
            self::STATE_FREE,
        ];
    }
}