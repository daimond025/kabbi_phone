<?php

return [
    'dispatcherService' => [
        'class' => \api\modules\v1\components\DispatcherService::className(),
    ],
    'loggerTarget'      => [
//        'class'    => \api\modules\v1\components\logger\target\SyslogTarget::className(),
        'class'    => \api\modules\v1\components\logger\target\FakeTarget::className(),
        'identity' => app()->params['identity'] . '_' . app()->params['version'],
    ],
    'logger'            => [
        'class'  => \api\modules\v1\components\logger\Logger::className(),
        'target' => 'loggerTarget',
    ],
];


