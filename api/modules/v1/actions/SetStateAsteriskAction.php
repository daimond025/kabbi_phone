<?php

namespace api\modules\v1\actions;

use api\modules\v1\exceptions\BadParamsException;
use api\modules\v1\exceptions\DispatcherNotFoundException;
use api\modules\v1\models\user\UserDispatcher;
use api\modules\v1\requests\SetStateAsteriskRequest;

/**
 * Class SetStateAsteriskAction
 * @package api\modules\v1\actions
 *
 */
class SetStateAsteriskAction extends BaseAction
{
    protected function getRequest()
    {
        return new SetStateAsteriskRequest();
    }

    protected function execute()
    {
        if ($this->request->requestId) {
            app()->get('apiLogger')->setId($this->request->requestId);
        }

        app()->get('apiLogger')->error('Установка статуса оператора.');

        if($this->request->getErrors()) {
            app()->get('apiLogger')->error('Ошибка валидации входящих параметров');
            throw new BadParamsException();
        }

        /** @var $dispatcher UserDispatcher */
        $dispatcher = $this->findDispatcherModel($this->request->agent);
        if (!$dispatcher) {
            app()->get('apiLogger')->error('Оператор {agent} не найден', ['agent' => $this->request->agent]);
            throw new DispatcherNotFoundException();
        }

        switch ($this->request->state) {
            case SetStateAsteriskRequest::STATE_AVAILABLE:
                app()->dispatcherService->setShift($dispatcher->user_id, $dispatcher->user->tenant_id);
                break;
            case SetStateAsteriskRequest::STATE_UNAVAILABLE:
                app()->dispatcherService->setPause($dispatcher->user_id, $dispatcher->user->tenant_id, $this->request->reason);
                break;
            case SetStateAsteriskRequest::STATE_FREE:
                app()->dispatcherService->setFree($dispatcher->user_id, $dispatcher->user->tenant_id);
                break;
            case SetStateAsteriskRequest::STATE_BUSY:
                app()->dispatcherService->setBusy($dispatcher->user_id, $dispatcher->user->tenant_id);
                break;
        }

        $this->response->setContent(['result' => 1]);
    }

    /**
     * @param $sip
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    protected function findDispatcherModel($sip)
    {
        return UserDispatcher::find()->where(['sip_id' => $sip])->limit(1)->one();
    }
}



