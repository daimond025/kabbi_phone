<?php

namespace api\modules\v1\actions;

use api\modules\v1\components\ErrorCode;
use api\modules\v1\components\Response;
use api\modules\v1\exceptions\BadParamsException;
use api\modules\v1\exceptions\DispatcherNotFoundException;
use api\modules\v1\exceptions\RedisDbException;
use api\modules\v1\requests\BaseRequest;
use yii\base\InvalidConfigException;
use yii\base\Action;

/**
 * Class BaseAction
 * @property BaseRequest $request
 * @property Response    $response
 *
 * @package api\modules\v1\actions
 */
abstract class BaseAction extends Action
{
    protected $request;
    protected $response;


    protected function execute()
    {
        throw new InvalidConfigException();
    }

    protected function setRequest()
    {
        throw new InvalidConfigException();
    }

    protected function getRequest()
    {
        throw new InvalidConfigException();
    }

    public function run()
    {
        try {
            $this->request = $this->getRequest();
            $this->request->validate();
            $this->response = new Response();

            $this->execute();

        } catch (BadParamsException $ex) {
            $this->response->setCode(ErrorCode::BAD_PARAM);

        } catch (DispatcherNotFoundException $ex) {
            $this->response->setCode(ErrorCode::DISPATCHER_NOT_FOUND);

        } catch (RedisDbException $ex) {
            $this->response->setCode(ErrorCode::REDIS_DB_EXCEPTION);

        } catch (\Exception $ex) {
            //            \Yii::error($ex);
            $this->response->setCode(ErrorCode::INTERNAL_ERROR);

        }
        finally {
            $response = $this->response->getResponse();

            app()->get('apiLogger')->log('Response: {data}',
                ['data' => json_encode($response, JSON_UNESCAPED_UNICODE)]);

            return $response;
        }
    }

    /**
     * Getting value of server param
     *
     * @param string $param
     * @param string $defaultValue
     *
     * @return string
     */
    protected function getServerParam($param, $defaultValue = '')
    {
        return array_key_exists($param, $_SERVER) ? $_SERVER[$param] : $defaultValue;
    }

    /**
     * @return string
     */
    protected function getSender()
    {
        $ip = empty($this->getServerParam('HTTP_X_REAL_IP'))
            ? $this->getServerParam('REMOTE_ADDR', '???')
            : $this->getServerParam('HTTP_X_REAL_IP');

        $port = empty($this->getServerParam('HTTP_X_REAL_PORT'))
            ? $this->getServerParam('REMOTE_PORT', '???')
            : $this->getServerParam('HTTP_X_REAL_PORT');

        return implode(':', [$ip, $port]);
    }

}