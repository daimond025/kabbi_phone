<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\SetStateAsteriskAction;
use api\modules\v1\Module;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class ApiController
 * @package api\modules\v1\controllers
 *
 * @property Module $module
 */
class ApiController extends Controller
{
    public function actions()
    {
        return [
            'set-state-asterisk' => [
                'class' => SetStateAsteriskAction::className(),
            ],
        ];
    }

    public function actionIndex()
    {
        return 'index';
    }

    public function actionError()
    {
        return 'error';
    }

    public function actionVersion()
    {
        return 'v' . app()->params['version'];
    }

    public function actionPing()
    {
        return ['time' => time()];
    }
}