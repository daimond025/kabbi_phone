<?php

namespace api\modules\v1;

use api\modules\v1\components\DispatcherService;

/**
 * Class Module
 * @package api\modules\v1
 *
 * @property DispatcherService $dispatcherService
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\v1\controllers';

    public function init()
    {
        parent::init();

        $components = require __DIR__ . '/components.php';
        $this->addComponents($components);

    }

    protected function addComponents($components)
    {
        foreach ($components as $id => $component) {
            app()->set($id, $component);
        }
    }
}