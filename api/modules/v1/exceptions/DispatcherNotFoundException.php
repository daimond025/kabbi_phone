<?php

namespace api\modules\v1\exceptions;

use yii\base\Exception;

class DispatcherNotFoundException extends Exception
{

}