<?php

namespace api\modules\v1\components;

use api\modules\v1\exceptions\RedisDbException;
use api\modules\v1\models\DispatcherLiveNotice;
use api\modules\v1\models\FreeDispatcherQueue;
use yii\base\Object;

class DispatcherService extends Object
{
    /**
     * Поставить оператора на паузу
     * @param int $userId
     * @param int $tenantId
     *
     * @return bool
     * @throws RedisDbException
     */
    public function setPause($userId, $tenantId, $reason = '')
    {
        app()->get('apiLogger')->log('Ставим оператора id {userId} на паузу', ['userId' => $userId]);

        $liveNotice = DispatcherLiveNotice::findOne($userId);
        if ($liveNotice) {
            app()->get('apiLogger')->log('Редактируем прямое уведомление оператору id {userId}', ['userId' => $userId]);
        } else {
            app()->get('apiLogger')->log('Создаем прямое уведомление оператору id {userId}', ['userId' => $userId]);
            $liveNotice = new DispatcherLiveNotice();
            $liveNotice->id = $userId;
        }
        $liveNotice->action = DispatcherLiveNotice::SET_PAUSE;
        $liveNotice->message = $reason;

        if (!$liveNotice->save()) {
            app()->get('apiLogger')->error('Ошибка сохранения прямого уведомления оператору id {userId}', ['userId' => $userId]);
            throw new RedisDbException();
        }

        $this->emptyQueue($userId, $tenantId);
        return true;
    }

    /**
     * Снять диспетчера с паузы
     * @param int $userId
     * @param int $tenantId
     *
     * @return bool
     * @throws RedisDbException
     */
    public function setShift($userId, $tenantId)
    {
        app()->get('apiLogger')->log('Снимаем оператора id {userId} с паузы', ['userId' => $userId]);

        $liveNotice = DispatcherLiveNotice::findOne($userId);
        if ($liveNotice) {
            app()->get('apiLogger')->log('Редактируем прямое уведомление оператору id {userId}', ['userId' => $userId]);
        } else {
            app()->get('apiLogger')->log('Создаем прямое уведомление оператору id {userId}', ['userId' => $userId]);
            $liveNotice = new DispatcherLiveNotice();
            $liveNotice->id = $userId;
        }
        $liveNotice->action = DispatcherLiveNotice::SET_SHIFT;
        $liveNotice->message = null;

        if (!$liveNotice->save()) {
            app()->get('apiLogger')->error('Ошибка сохранения прямого уведомления оператору id {userId}', ['userId' => $userId]);
            throw new RedisDbException();
        }

        $this->pushToQueue($userId, $tenantId);
    }

    /**
     * Установить диспетчера на статус "занят"
     * @param $userId
     * @param $tenantId
     */
    public function setBusy($userId, $tenantId)
    {
        app()->get('apiLogger')->log('Ставим оператора id {userId} на "занят"', ['userId' => $userId]);
        $this->emptyQueue($userId, $tenantId);
    }

    /**
     * Установить диспетчера на статус "свободный"
     * @param $userId
     * @param $tenantId
     */
    public function setFree($userId, $tenantId)
    {
        app()->get('apiLogger')->log('Ставим оператора id {userId} на "свободный"', ['userId' => $userId]);
        $this->emptyQueue($userId, $tenantId);
    }






    protected function emptyQueue($userId, $tenantId)
    {
        $freeDispatchers = FreeDispatcherQueue::findOne($tenantId);
        if ($freeDispatchers) {
            if ($freeDispatchers->lrem($userId)) {
                app()->get('apiLogger')->log('Удаляем оператора id {userId} из очереди свободных диспетчеров', ['userId' => $userId]);
            } else {
                app()->get('apiLogger')->error('Ошибка удаления оператора id {userId} из очереди свободных диспетчеров', ['userId' => $userId]);
            }
        }
    }

    protected function pushToQueue($userId, $tenantId)
    {
        $this->emptyQueue($userId, $tenantId);
        $freeDispatchers = FreeDispatcherQueue::findOne($tenantId);
        if (!$freeDispatchers) {
            $freeDispatchers = new FreeDispatcherQueue();
            $freeDispatchers->tenantId = $tenantId;
        }
        if ($freeDispatchers) {
            if ($freeDispatchers->rpush($userId)) {
                app()->get('apiLogger')->log('Добавляем оператора id {userId} в конец очереди свободных диспетчеров', ['userId' => $userId]);
            } else {
                app()->get('apiLogger')->error('Ошибка добавления оператора id {userId} в конец очереди свободных диспетчеров', ['userId' => $userId]);
            }
        }
    }

}