<?php

namespace api\modules\v1\components;

class ErrorCode
{
    const OK = 0;

    // Внутренние ошибки
    const INTERNAL_ERROR = 1;
    const METHOD_NOT_FOUND = 2;
    const NEED_REAUTH = 3;

    // Ошибки входящих параметров
    const BAD_PARAM = 100;
    const MISSING_INPUT_PARAMETER = 101;

    // Ошибки с БД
    const EMPTY_DATA_IN_DATABASE = 200;
    const DISPATCHER_NOT_FOUND = 201;
    const REDIS_DB_EXCEPTION = 202;

}