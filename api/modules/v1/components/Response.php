<?php

namespace api\modules\v1\components;

use api\modules\v1\exceptions\InvalidErrorCode;
use yii\base\Object;

/**
 * Class Response
 *
 * @property integer $code
 * @property mixed   $content
 *
 * @package api\modules\v1\components
 */
class Response extends Object
{
    protected $code;
    protected $content;


    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->code    = ErrorCode::OK;
        $this->content = [];
    }

    /**
     * Получить $code
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }


    /**
     * Получить $content
     *
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Установить $code
     *
     * @param integer $code
     */
    public function setCode($code)
    {
        if ($code !== ErrorCode::OK) {
            $this->setContent(null);
        }
        $this->code = $code;
    }

    /**
     * Установить $content
     *
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getResponse()
    {
        return $this->code === ErrorCode::OK ? $this->content : ['error' => $this->code];
    }
}