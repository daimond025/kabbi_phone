<?php

namespace api\modules\v1\models;


use api\extensions\redis\string\ActiveRecord;

class DispatcherLiveNotice extends ActiveRecord
{

    const SET_PAUSE = 'set_pause';
    const SET_SHIFT = 'set_shift';

    public static function getDb()
    {
        return \Yii::$app->get('redis_dispetcher_live_notice');
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function attributes()
    {
        return ['id', 'action', 'message'];
    }

    public function rules()
    {
        return [
            [['id', 'action'], 'required'],
            ['action', 'in', 'range' => self::getActionList()],
            ['message', 'safe'],
        ];
    }

    public static function getActionList()
    {
        return [
            self::SET_SHIFT,
            self::SET_PAUSE,
        ];
    }
}