<?php

namespace api\modules\v1\models\user;

use yii\db\ActiveRecord;

/**
 * Class User
 * @package api\modules\v1\models\user
 *
 * @property int $dispetcher_id
 * @property int $user_id
 * @property string $sip_id
 *
 * @property int $dispatcher_id
 * @property User $user
 */

class UserDispatcher extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_dispetcher}}';
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    public function getDispatcher_id()
    {
        return $this->dispetcher_id;
    }

    public function setDispatcher_id($value)
    {
        $this->dispetcher_id = $value;
    }

}