<?php

namespace api\modules\v1\models\user;

use yii\db\ActiveRecord;

/**
 * Class User
 * @package api\modules\v1\models\user
 *
 * @property int $user_id
 */

class User extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
}