<?php

namespace api\exceptions;

use yii\web\HttpException;

class EmptyParamsException extends HttpException
{
    protected $message = 'empty params';

    public function __construct()
    {
        parent::__construct(200, $this->message);
    }

}