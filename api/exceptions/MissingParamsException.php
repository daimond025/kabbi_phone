<?php

namespace api\exceptions;

use yii\web\HttpException;

class MissingParamsException extends HttpException
{
    protected $message = 'missing params';

    public function __construct()
    {
        parent::__construct(200, $this->message);
    }

}