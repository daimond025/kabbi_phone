<?php

namespace api\exceptions;

use yii\web\HttpException;

class UnknownPbxHostException extends HttpException
{
    protected $message = 'unknown pbxHost';

    public function __construct()
    {
        parent::__construct(200, $this->message);
    }

}