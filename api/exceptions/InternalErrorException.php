<?php

namespace api\exceptions;

use yii\web\HttpException;

class InternalErrorException extends HttpException
{
    protected $message = 'internal error';

    public function __construct()
    {
        parent::__construct(200, $this->message);
    }

}