# Gootax - api phone call

## 1.9.0 (20.08.2018)
 - [#6497](https://red.gootax.pro/issues/6497) - для арендатора 4148 voyage переопределена отправка уведомления клиенту через внешний сервер

## 1.8.0 (02.08.2018)
 - [#6328](https://red.gootax.pro/issues/6328) - добавлен новый метод get_notify_data

## 1.7.0 (20.07.2018)
 - [#6284](https://red.gootax.pro/issues/6284) - при поппыте отзвона по заказу берется только линия подходящая по тарифу

## 1.6.0 (26.06.2018)
 - [#6076](https://red.gootax.pro/issues/6076) - для арендатора 2083 переопределена отправка уведомления клиенту через внешний сервер
 - [#6042](https://red.gootax.pro/issues/6042) - исправлена ошибка с воспроизведением цвета авто, когда не найден аудиофайл с цветом

## 1.5.0 (19.06.2018)
 - [#6009](https://red.gootax.pro/issues/6009) - для арендатора 3083 переопределена отправка уведомления клиенту через внешний сервер

## 1.4.0 (18.04.2018)
 - [#5628](https://red.gootax.pro/issues/5628) - добавлены настройки для конфигурирования кеша схемы бд и префикса ключа кеша    
 - Yii2 update 2.0.11.2->2.0.12.2

## Версия 1.3

 - [#5193](https://red.gootax.pro/issues/5193) - Добавлены два поля в notify_client: company_phone и order_number

## Версия 1.2

 - [#3080](https://red.gootax.pro/issues/3080) - Добавлен новый сценарий doRedirect, переадресация вызова.
 - Добавлен новый метод для изменения статуса оператора.
 - Переписан компонент apiLogger.
 

## Версия 1.1.4

 - [#3827](https://red.gootax.pro/issues/3827)  - Убрана а авто постановка оператора на паузу, если он не взял трубку


## Версия 1.1.3

 - Добален новый сервер астериска 37.26.63.168 в конфигах

## Версия 1.1.2

 - Добален новый сервер астериска 62.212.235.118 в конфигах